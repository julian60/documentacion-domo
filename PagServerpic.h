/**
******************************************************
* @file PagServerpic.h
* @brief Documentacion de SrverPic
* @author FabLab Lleida
* @author Jose Martinez Molero
* @author Julian Salas Bartolome
* @version 1.0
* @date Abril 2018
*
*
*******************************************************/

/**
 * \section IndiceServerPic Indice ServerPic
 *
 *    - \ref ServerPic1
 *			- \ref ServerPic11
 *			- \ref ServerPic12
 *			- \ref ServerPic13
 *			- \ref ServerPic14
 *			- \ref ServerPic15
 *
 * \section ServerPic1 2.- Central. ServerPIC
 *
 * \subsection ServerPic11 2.1 - Hardware
 *
 * Como centralita del sistema se va a utilizar un servidor ( ServerPic ) con capacidad para facilitar la comunicación entre sensores, actuadores e interfaces.
 * 
 * ServerPic se puede instalar en cualquier sistema que soporte Java y disponga de un servidor mysql. Para nuestro sistema hemos optado por utilizar una Raspberry Pi 3 o Raspberry Pi Zero.
 *
 * La Raspberry se conecta a la SSID doméstica con una IP estática y todos los dispositivos se conectarán a ServerPic a través de la misma SSID, esta forma de trabajo nos permitirá también telecontrolar el sistema desde internet.
 *
 * Donde no haya acceso a internet, el sistema también puede ser utilizado pero no podrá ser controldado desde el exterior, su uso se limita a la misma residencia. Para este modo de funcionamiento se puede utilizar un router que tenga wifi.
 * También se puede configurar el WIFI de la Raspberry como AP, en este caso los módulos se conectarán a la SSID creada con la Raspberry
 * 
 *
 * \subsection ServerPic12 2.2 - Software
 * ServerPic es un servidor realizado en Java que permite la conexion de clientes usando sockets.
 *
 * El servidor se desarrolló hace ya unos cuantos años, actualmente se esta haciendo una revisión del mismo para optimizarlo,  no obstante, a lo largo de estos años se ha comprobado su estabilidad y ha cumplido sobradamente con los objetivos para los que fué creado 
 *
 * Cualquier dispositivo capaz de abrir un socket se puede conectar al servidor e iteractuar con el
 *
 * Cuando un Cliente se conecta a ServerPic, este le envia el texto <b>CONECTADO</b>, el Cliente, cuando recibe este texto le debe responder al servidor
 * con el nombre de Cliente, este nombre será su identificador en el sistema. Una vez el Cliente envia el nombre al Servidor, si todo es correcto
 * este le responde con el texto <b>Registrado</b>, a partir de ese momento, el Cliente forma parte del sistema y puede iteractuar con el.
 *
 * En la siguiente imagen se observa una sesion de putty abierta con el servidor con el nombre de Julian
 *
 * \image html Documentacion\Imagenes\ConexionaServerPic.png
 *

 * Los Clientes, una vez conctados tienen acceso a una serie de funcionalidades que les ofrece el Servidor, estas funcionalidades se han ido modificando y sustituyendo en función del proyecto en el que se haya utilizado el Servidor.
 *
 * Para este proyecto, las funcionalidades habilitadas para los clientes conectados al servidor son las siguientes:
 *
 *		mensaje-:-<Cliente>-:-Texto Mensaje<intro> .- Envia 'Texto Mensaje' al cliente conectado con nombre 'Cliente'  
 *                                                       		El cliente recibe el siguente mensaje <Remitente>-:-Texto Mensaje. 
 *                                                       		Si <Cliente> no esta en ese momento conectado se guarda el mensaje hasta que se conecte
 *		hora<intro>                                .- Recibe la hora del servidor
 *		latido<intro>                              .- Recibe el texto 'Ok' del servidor
 *		ping-:-<Cliente>                           .- Recibe 'Pong' desde 'Cliente' si este esta conectado. 			 
 *		load-:-<NombreVariable>-:-valor            .- Guarda 'Valor' en la variable 'NombreVariable' para el cliente que lo solicita
 *		save-:-<NombreVaribable>                   .- El servidor envia el 'valor' alamcenado para este cliente en 'NombreVariable'
 *
 * Segun se vaya avanzando en el proyecto, si aparecen nuevas necesidades para el servidor, se pueden ir incorporando.
 *
 * La funcionalidad que más se va a utilizar es <b>mensaje</b>
 *
 * Si el Cliente <b>Julian</b> quiere mandar un saludo al Cliente <b>Pepe</b>, <b>Julian</b> mandará lo siguiente
 *
 *		mensaje-:-Pepe-:-Hola Pepe, como estas?
 * 
 *
 * <b>Pepe</b> recibirá el saludo precedido por el remitente
 *
 *		Julian-:-Hola Pepe, como estas?
 *
 * En la siguiente imagen se observa ese dialogo entre dos Clientes conectados a ServerPic.
 *
 * También se observa como el Cliente Julian manda el comando <b>Hora</b> al servidor y este le responde o como Julian manda el texto <b>Latido</b> al
 * Servidor y este le responde <b>Ok</b>. 
 *
 * \image html Documentacion\Imagenes\Dialogo.png
 *
 * Como se dijo, un Cliente podrá ser cualquier cosa capaz de abrir un socket. Entre 'culaquier cosa' se contemplan los sensores y actuadores por un lado y por otro 
 * los ordenadores, tablets, teléfonos que abrirán socket en el Servidor mediante putty, php, Javascript... para hacer de interfaces del sistema.
 *
 * Para instalar ServerPic en una Raspberry es necesario una preparación previa para que al final ServerPic pueda ser operativo, para ello se ofrecen las siguientes posibilidades:
 *
 * - Manual.-  Toda la preparación y la propia instalación de forma manual esta explicado en \ref PaginaRaspBerry0 .
 * - AutoInstalable.- Un fichero Bash junto a uno de configuración se encargan de todas las tareas de instalación según se describe en \ref PaginaRaspBerry4 . 
 *
 * Todo lo que se detalla en esos enlaces no tiene por que ser lo más correcto, es más, somos conscientes de que cometemos errores, nuestros conocimientos de Linux y Raspberry son realmente escasos, practicamente nulos 
 * y toda la preparación  se ha realizado por consejos y aportaciones de terceros y sobre todo, por horas de dedicación y ganas de sacar esto adelante.
 *
 * \subsection ServerPic13 2.3 - Acceso Web a ServerPic
 * 
 * Se ha previsto un acceso mediante web a ServerPic para poder telecontrolar los dispositivos conectados al servidor. 
 * 
 * Se trata de presentar en una pagina web una serie de iconos que, mediante cliks de ratón puedan enviar mensajes a ServerPic. 
 * 
 * Las primeras pruebas se realizaron con JavaScritp + Ajax y  pesar de que el funcionamiento en la dirección Web -> Servidor fue correcto
 * se necesitaba tambien la comunicación en la otra dirección ( Servidor -> Web ) para presentar en el navegador el estado de los dispositivos.
 * 
 * Al principio se probaron peticiones periodicas mediante Ajax pero se descartó esta operativa por que al abrir multiples págnas, la actualizacion de las páginasse no era inmediata y se cargaba el 
 * servidor de forma innecesaria .
 * 
 * Después de buscar y buscar descubrimos la teconología Web Socket que permite la comunicacion bidireccional entre página y servidor 
 * utilizando un scoket.
 * 
 * Para conseguirlo se ha utilizado un servidor de Sockets en PHP, posiblemenre se podría integrar directamente en ServerPic mediante Java pero
 * en un primer intento vamos a desarrollarlo a partir del servidor PHP.
 * 
 * Este servidor (WebSocket) hace de puente entre ServerPic y una pagina html. Cuando arranca el servidor WebSocket, establece, por un lado, una conexión con ServerPic como 
 * un cliente más, su identificador en ServerPic es WebSocket, por otro lado, queda a la escucha de peticiones ws (web socket), para cada navegador que hace una 
 * solicitud ws,  se establece un socket de forma que, cualquier informacion que el servidor WebSocket recibe de ServerPic, se tresmite  a todas
 * las páginas con socket abierto y cualquier información que se reciba desde una página conectada a  WebSocket, se retransmite al resto de páginas y a ServerPc.
 * 
 * De esta forma, se consigue acceder mediante Web a ServerPic. Más adelante, se incluirán sencillos ejemplos para mandar mensajes desde una web
 * a los dispositvos conectados a ServerPic
 *
 *   \image html Documentacion\Imagenes\WebSocket.png
 *
 * \subsection ServerPic14 2.4 - HomeKit
 *
 * Se ha previsto también el acceso a los accesorios conectados a Serverpic mediante el HomeKit de Apple
 * apoyandonos el HAP_Node
 *
 * \subsection ServerPic54 2.5 - GoogleHome
 *
 * Se ha previsto también el acceso a los accesorios conectados a Serverpic mediante el Google Assitent
 * 
 * Para continuar vamos a hablar de \ref TransductoresActuadores o podemos volver al \ref Indice
 *
 */
 //-------------------------------------------------------
//Pagina2 ServerPic 
//-------------------------------------------------------
/**
 * \page ServerPic 2.- ServerPic
 *
 */