/**
******************************************************
* @file RaspberryPreparacionAutoInstall.h
* @brief Documentacion para preparar una Raspberry para utilizarla como servidor PARA DISPOSITIVOS DOMOTICOS
* @author Julian Salas Bartolome
* @version 1.0
* @date Mayo 2018
*
*
*******************************************************/

//-------------------------------------------------------
//Este documento NO TIENE Codigo, simplemente contiene documentacion para Doxygen  
//-------------------------------------------------------

/**
* Otra forma más sencilla de instalar ServerPic en una Raspberry es utilizar un fichero bash donde están automatizadas todas las tareas detalladas en \ref PaginaRaspBerry0 . 
*
* Es una forma sencilla y rápida de hacerlo pero es muy conveniente conocer el proceso manual pues, es posible que pueda haber cambios en las versiones de software que puedieran provocar
* el mal funcionamiento del proceso bash y, en ese caso, conociendo el proceso manual, podría actualizarse sin problema el ficehro bash.
*
* El bash va dando instrucciones para ir instalando y configurando todos los paquetes.
*
* El bash, <b>Instalacion.sh</b>, carga un fichero de configuración <b>Configuracion</b> donde se especifica los paquetes a cargar y los parametros de configuración de esos paquetes:
*
* Hay paquetes que no tienen nada que ver con ServerPic pero, como se han instalado en la Raspberry de prueba, se han incluido en la autoinstalación y cada ususario podrá decidir si se instala o no.
* 
*
* Los distintos apartados del fichero de configuración son los siguientes
*
* \section SeccionRaspBerry31 1.- Password ususario pi
*
*		PasswordRasp=FabLab25?
*
* Cambia el password del usuario pi de la raspberry, si se omite esta línea no se cambia el password
* 
* \section SeccionRaspBerry32 2.- Puero SSH
*
*		PortSSH=1222
* 
* Cambia el puerto para SSH, si se omite esta línea no se cambia el puerto
* 
* \section SeccionRaspBerry33 3.- Modo STA
*
*		ModoRed=STA
*		NombreSSID=FABLAB_LLEIDA
*		PasswordSSID=FABLAB2015
*		IPETH0=192.168.1.12
*		IPWLAN0=192.168.1.22
*
* La Raspberry se pone en mod STA, tanto etho como wlan0 se pueden conectar a una red existente.
*
* Si se omite NombreSSID o PasswordSSID,  no se modifica la SSID existente en la raspberry.
*
* Si se pone una dirección en IPETH0, se pondrá una ip estática en eth0. Si se omite se mantendrá la configuracion para eth0.
*
* Si se pone una dirección en IPWLAN0, se pondrá una ip estática en wlan. Si se omite se mantendrá la configuración para wlan0.
* @warning NO SE COMPRUEBAN POSIBLES ERRORES DE ASIGNACION. LA RED DEBE SER 192.168.1.X
*
* \section SeccionRaspBerry34 4.- Modo AP
*
*		ModoRed=AP
*		IPETH0=192.168.1.12
*		IPWLAN0=192.168.2.1
*		NombreSSID=FABLAB_LLEIDA
*		PasswordSSID=FABLAB2015
*
* La wlan de la Raspberry se pone en modo AP.  eth0,  Si se incluye la linea IPETHO=192.168.1.X  se conectará a una red existente.
*
* Es necesaria una direccion para IPWLAN0. Si se omite no se pondra en modo AP  
*
* @warning NO SE COMPRUEBAN POSIBLES ERRORES DE ASIGNACION. LA RED DEBE etho SER 192.168.1.X
*
* \section SeccionRaspBerry35 5.- screen
*
*		Screen=Si
*
* Si se pone esa línea se instalará screen , si se omite no se instalará.
*
* \section SeccionRaspBerry36 6.- Samba
*
*		PasswordSamba=FabLab25?
*
* Si se pone esa línea, se instala samba con ese password para el ususario pi, si se omite no se inatalará samba
*
*
* \section SeccionRaspBerry37 7.- Apache
*
*		Apache=Si
*		PortApache=1280
*
* Si se quiere instalar Apache, se debe incluir la línea Apache=Si.
*
* Se se pone la línea PortApache=XXXX, se asigna ese Puerto a Apache
*
* \section SeccionRaspBerry38 8.- PHP
*
*		PHP=Si
*
* Si se pone esa línea se instala PHP
*
* \section SeccionRaspBerry39 9.- mysql
*
*		Mysql=Si
*		PasswordRootMysql=FabLab25?
*
* Para instalar mysql se debe incluri la líne Mysql=Si
*  
* Con PasswordRootMysql se asigna un password al ususario root
*
* \section SeccionRaspBerry310 10.- phpmyadmin
*
* 		PhpMyAdmin=Si
*
* Para instalar phpmyadmin se debe incorporar esa línea
*
* \section SeccionRaspBerry311 10.- Java
*
*		Java=Si
*		VersionJava=oracle-java8-jdk
*
* Con la línea Java=si, se instalará la versión de Java que se detalla en la siguiente línea (VersionJava) 
* 
* Si se omite la versión, por defecto, se instalará oracle-java8-jdk
*
* \section SeccionRaspBerry312 11.- ServerPic
*
*		ServerPic=Si
*		PuertoServerPic=1201
*
* Aunque el objetivo de todo esto es instalar ServerPic, se deja como opción su instalación.
* 
* 
* Como viene siendo habitual a lo largo de todo el documento,para instalar ServerPic se debe poner la línea ServerPic=Si. ServerPic escuchará en el puerto 2001
* a no ser que se indique otro puerto en la variable PuertoServerPic.
*
*
* \section SeccionRaspBerry313 12.- WebSocket
* 
* 	WebSocket=Si
*
* WebSocket es 'otro' servidor realizado en php que permitira el acceso a ServerPic mediante la tecnologia Web Socket
* 
* Si queremos que se instale, se debe poner la linea WebSocket=Si y escuchará en el puerto PuertoServerPic + 1
*	
*
* \section SeccionRaspBerry31 13.- Dyndns
*
*		UsuarioDynDns=FabLab25
*		PasswordDynDns=FABLAB2015
*		DominioDynDns=fablab.gotdns.com
*
* Para poder acceder a ServerPic desde internet, es necesario recurrir a un servicio de dns. Si se ponen los datos de estas líneas se accederá a dyndns
*
* Es necesario tener una cuenta en dyndns y proporcionar usuario, password y dominio de nuestra cuenta
*
* \section SeccionRaspBerry315 14.- owncloud
*
*		ownCloud=Si
*		PathownCloudData://192.168.1.2/share /Dir_ownCloud cifs ,defaults,uid=www-data,gid=www-data,user=admin,password=JUSABAROMEAR,file_mode=0770,dir_mode=0770 0 0
*		UsuariomysqlownCloud=julian
*		PasswordmysqlownCloud=Jusaba2408
*
* Aunque owncloud no tiene nada que ver con ServerPic, se deja aqui la opción para su instalación automática
*
* Si se omite lal inea ownCloud=Si o falta usuario o password, no se instalará. Si se incluye en PathownCloudData el acceso a un disco de red, 
* se almacenaran alli los datos, en caso contrario se hara una instalación básica.	
*
* Una vez que hemos preparado la Raspberry y con ServerPic funcionando, es el momento de volver al \ref Indice Principal para continuar con los transductores y actuadores.
*/
//-------------------------------------------------------
//Pagina1 Instalacion de ServerPic en Raspberry
//-------------------------------------------------------
/**
 * \page PaginaRaspBerry4 Autoinstalacion  de ServerPic en Raspberry
 *
 */
