/**
******************************************************
* @file RaspberryPreparacionManual.h
* @brief Documentacion para preparar una Raspberry para utilizarla como servidor PARA DISPOSITIVOS DOMOTICOS
* @author Julian Salas Bartolome
* @version 1.0
* @date Octubre 2017
*
*
*******************************************************/



//-------------------------------------------------------
//Este documento NO TIENE Codigo, simplemente contiene documentacion para Doxygen  
//-------------------------------------------------------

//-------------------------------------------------------
//Pagina principal  de  la preparacion de Raspberry
//-------------------------------------------------------
/**
 *
 * \section IndiceInstacionManualRaspberry Instalacion Manual
 *
 * En este documento se describe el procedimeinto para preparar una Raspberry para instalar ServerPic
 *
 *    - \ref SeccionRaspBerry1
 *         - \ref SubSeccionRaspBerry11
 *        	 - \ref SubSeccionRaspBerry111
 *        	 - \ref SubSeccionRaspBerry112
 *         - \ref SubSeccionRaspBerry12
 *        	 - \ref SubSeccionRaspBerry121
         	 - \ref SubSeccionRaspBerry122
 *        	 - \ref SubSeccionRaspBerry123
 *	        	 - \ref SubSeccionRaspBerry1231
 *	        	 - \ref SubSeccionRaspBerry1232   
 *        	 - \ref SubSeccionRaspBerry124   
 *        	 - \ref SubSeccionRaspBerry125         
 *    - \ref SeccionRaspBerry2
 *         - \ref SubSeccionRaspBerry21
 *        		 - \ref SubSeccionRaspBerry211
 *        		 - \ref SubSeccionRaspBerry212
 *        		 - \ref SubSeccionRaspBerry213 
 *	        		 - \ref SubSeccionRaspBerry2131 
 *	        		 - \ref SubSeccionRaspBerry2132 
 *	        		 - \ref SubSeccionRaspBerry2133 
 *	        		 - \ref SubSeccionRaspBerry2134
 *	        		 - \ref SubSeccionRaspBerry2135
 *        		 - \ref SubSeccionRaspBerry214
 *         - \ref SubSeccionRaspBerry22
 *        		 - \ref SubSeccionRaspBerry221
 *
 * Vuelta a \ref Indice Principal
 */

//-------------------------------------------------------
//Pagina1 Instalacion de ServerPic en Raspberry
//-------------------------------------------------------
/**
 * \page PaginaRaspBerry0 Instalacion Manual de ServerPic en Raspberry
 *
 */

//-------------------------------------------------------
//Pagina2  Puesta en Marcha  
//-------------------------------------------------------
//-------------------------------------------------------
//Pagina2 Seccion 1
//		1 Puesta en Marcha  
//			1.1 Instalacion del sistema operativo
//				1.1.1 Descarga del sistema operativo
//				1.1.2 Grabar sistema operativo en SD
//-------------------------------------------------------
/**
 * \section SeccionRaspBerry1 1.- Puesta en marcha
 * \subsection SubSeccionRaspBerry11		1.1.- Instalaci�n del sistema operativo 
 * \subsection SubSeccionRaspBerry111		1.1.1- Descarga del sistema operativo 
 *
 * Se debe descargar desde el  <a href="https://www.raspberrypi.org/downloads/raspbian/">Sitio oficial</a>	 
 *
 * Se puede descargar la versi�n de escritorio o la versi�n LITE. Para este proyecto es aconsejable utilizar una versi�n sin escritorio para optimizar el espacio ocupado por el SO.
 *
 * \subsection SubSeccionRaspBerry112	1.1.2.- Grabar el Sisitema operativo en SD.
 * Extraer el fichero imagen y grabarlo en un SD mediante <a href="file:..\..\Doxygen\html\Documentacion\Software\win32diskimager-1.0.0-install.exe">win32diskimager</a> o
 * con  <a href="file:..\..\Doxygen\html\Documentacion\Software\Etcher-1.0.0-win32-x64.exe">etcher</a>.
 *
 * Para poder acceder a la Raspberry por SSH es necesario habilitarlo, la manera m�s sencilla es a�adiendo al direcotrio raiz de la SD preparada  en el paso anterior un fichero completamente vacio con el nombre de <b>ssh</b>.
 * Una vez incorporado ese fichero, se debe introducir la SD en la Raspberry y alimentarla para iniciar el proceso de puesta en marcha.
 * 
 */
//-------------------------------------------------------
//Pagina1 Seccion 2   
//		1.2 COnfiguraciones basicas
//			1.2.1 Cambio Password
//			1.2.2 Modificar puerto SSH
//			1.2.3 Configuracion Wifi
//				1.2.3.1 Wifi en Raspberry pi
//				1.2.3.2 Wifi en Raspberry pi zero
//			1.2.4 Asignacion IP Estatica
//			1.2.5 Actualizr repositorios
//-------------------------------------------------------
/**
 * \subsection SubSeccionRaspBerry12	1.2.- Configuraciones b�sicas
 * \subsection SubSeccionRaspBerry121 1.2.1.- Cambio password
 * Por defecto, la Raspberry tiene un password com�n para todas ellas, es aconsejable o, mejor dicho, imprescindible cambiar el password del ususario pi
 * Antes de nada, partimos de que se ha habilitado ssh y que la Raspberry se ha conectado al router por ethernet, en el caso de la zero, se habr� conectado por wifi tal como se detalla en la secci�n correspondiente. 
 * Conect�ndonos al router, averiguaremos que direcci�n IP se le ha asignado a la Raspberry y accederemos a una ventana de comandos mediante ssh. Para esta conexi�n utilizaremos los par�metros  por defecto:
 *
 *		Puerto: 22
 *		Usuario: pi
 *		Password: raspberry
 *
 * Una vez conectados por ssh tecleamos
 *
 *			sudo passwd
 *
 * Nos preguntar� nuevo pasword y nos pedir� confirmaci�n
 *
 * Si queremos habilitar el acceso mediante superusuario, teclear:
 *
 *		sudo passwd root
 *
 * Escribir y confirmar el password para root
 *
 * Una vez habilitado el acceso por ssh, podemos habilitarlo/deshabilitarlo tecleando
 *
 *
 *			sudo raspi-config
 *
 * Nos aparece una ventana con varias opciones.
 *
 * Seleccionamos la opci�n <b>5.-Interfacing Options</b>
 * \image html ..\..\Documentacion\Imagenes\HabilitarSSH_1.jpg
 * Seleccionamos la opci�n <b>P2.-SSH</b>
 * \image html ..\..\Documentacion\Imagenes\HabilitarSSH_2.jpg
 * Y pulsamos <b>Yes</b>
 * \image html ..\..\Documentacion\Imagenes\HabilitarSSH_3.jpg 
 *
 * Desde esta pantalla de configuraci�n tambi�n es posible cambiar el password y otros par�metros de nuestra raspberry.
 *
 * \subsection SubSeccionRaspBerry122 1.2.2.- Modificar puerto SSH
 * Como se ha dicho, el puerto por defecto para ssh es el 22, si se quiere cambiar, editamos el fichero  /etc/ssh/sshd_config tecleando:
 *
 *		sudo nano /etc/ssh/sshd_config
 *
 * modificamos la linea <b>port 22</b>, generalmente aparece comentada con una almohadilla #, borramos dicho car�cter, ponemos el nuevo puerto y guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
 *
 * \subsection SubSeccionRaspBerry123 1.2.3.- Configuraraci�n WIFI
 *
 * \subsection SubSeccionRaspBerry1231 1.2.3.1.- WIFI en raspberry pi
 *
 * Para raspberry pi, se debe editar el ficehro <b>/etc/network/interfaces</b>
 *
 *		sudo nano /etc/network/interfaces
 *
 * Nos aseguramos de que existen las siguientes l�neas, si no fuera as�, se deben crear
 *
 *		iface wlan0 inet manual
 *		wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
 *
 * Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
 *
 * Editamos ahora el ficehro <b>/etc/wpa_supplicant/wpa_supplicant.conf</b>
 * 
 *		sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
 *
 * Tecleamos la SSID y Password a la que se debe conectar la raspberry
 *
 *
 *		network={
 *			ssid="miSSID"
 *			psk="micontrase�a"
 *			key_mgmt=WPA-PSK	
 *		}
 *
 * Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
 *
 *
 * \subsection SubSeccionRaspBerry1232 1.2.3.2.- WIFI en raspberry pi zero
 *
 * En la Zero, la red wifi se habilita en el momento de iniciarla por primera vez grabando en el directorio raiz de la SD donde se encuentra la imagen del SO el fichero <b>wpa_supplicant.conf</b>. Al arrancar, el SO se 
 * encargar� de configurar la red wifi con los datos de ese fichero que debe contener la siguiente informaci�n:
 * 
 *		network={
 *			ssid="miSSID"
 *			psk="micontrase�a"
 *			key_mgmt=WPA-PSK	
 *		}
 *
 * \subsection SubSeccionRaspBerry124 1.2.4.- Asignaci�n IP est�tica
 * Para poder acceder de forma pr�ctica a la Raspberry por SSH es necesario que disponga de una IP fija, para ello:
 *
 * Editar el fichero <b>/etc/dhcpcd.conf</b> tecleando:
 *
 *			sudo nano /etc/dhcpcd.conf
 *
 * Le a�adimos las siguientes lineas para asignarle por ejemplo la direcci�n 192.168.1.20
 *
 *		interface eth0 
 *		static ip_address=192.168.1.20/24
 *		static routers=192.168.1.1
 *		static domain_name_servers=8.8.8.8
 *		static domain_search=8.8.4.4
 *
 * Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
 *
 * @warning Si lo que se quiere configurar es la WLAN, en lugar de <b>interface etho</b> se debe poner <b>interface wlan0</b>
 *
 * Editar el fichero <b>/etc/network/interfaces</b> tecleando:
 *
 *		sudo nano /etc/network/interfaces
 *
 * Nos aseguramos de que eth0 esta en manual
 *
 *		iface eth0 inet manual
 *
 * Para Wifi, las lineas seran las siguientes
 *
 *		iface wlan0 inet manual
 *		wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
 *
 *
 * Si es necesario lo modificamos y  salvamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
 *
 * Reiniciamos la Raspberry
 *
 *		sudo reboot
 *
 * y comprobamos que tiene la direcci�n ip asignada con
 *
 *		ifconfig
 * 
 * @warning Hay que tener en cuenta que para acceder por SSH, ahora hay una nueva IP y un nuevo puerto de SSH
 *
 * \subsection SubSeccionRaspBerry125 1.2.5.- Actualizar repositorios
 * Teclear
 *
 *			sudo apt-get -y update
 *
 *					y luego
 *
 *			sudo apt-get -y upgrade
 *
 * Puedes volver al indice \ref IndiceInstacionManualRaspberry o continuar con \ref SubSeccionRaspBerry21
 */


//-------------------------------------------------------
//Pagina1 Puesta en marcha  
//-------------------------------------------------------
/**
 * \page PaginaRaspBerry1 Puesta en Marcha
 *
 */

//-------------------------------------------------------
//Pagina2  Software Basico 
//-------------------------------------------------------
//-------------------------------------------------------
//Pagina2 Seccion 1 
//		2.1 Instalacion Software generico
//			2.1.1 Screen
//			2.1.2 Samba
//			2.1.3 LAMP
//				2.1.3.1 Apache
//				2.1.3.2	PHP
//				2.1.3.3 Mysql
//				2.1.3.4 Phpmyadmin
//			2.1.4 JAVA
//-------------------------------------------------------
/**
 * \section SeccionRaspBerry2 2.- Software B�sico
 */

/**
 * \subsection SubSeccionRaspBerry21	2.1.- Instalaci�n de software gen�rico
 *
 */

/**
 * \subsection SubSeccionRaspBerry211	2.1.1- Screen
 *
 * Cuando desde un terminal SSH se arranca un proceso normalmente este 'muere' cuando se cierra el terminal. En muchas ocasiones es deseable mantener el proceso en ejecuci�n a�n cerrando el terminal.
 * Una forma de conseguir esto es utilizando Screen. Screen es un gestor de sesiones que permite arrancar un proceso y cerrar el terminal manteniendo el proceso en funcionamiento
 *
 * Para instalarlo teclear:
 *
 *			sudo apt-get install screen
 *
 *
 * Algunos comandos �tiles
 *
 * Para ver las sesiones en marcha
 *
 *		screen -ls
 *
 * Para crear una sesi�n, simplemente se escribir�
 *
 * 		screen
 * 
 * realizamos las operaciones que necesitemos en la sesi�n y para cerrarla tecleamos <b>exit</b> o  <b><Ctrl>d</b>, con esto 'eliminaremos' la sesi�n, si lo que queremos es cerrarla pero manteni�ndola activa saldremos con  <b><Ctrl>a</b> + <b><Ctrl>d</b> 
 *
 * \image html ..\..\Documentacion\Imagenes\screen_1.jpg
 *
 * Para acceder a una sesi�n activa lo haremos con el comando <b>-r</b> o <b>-x</b>
 *
 *		screen -r 29698.pts-4.raspberrypi
 *
 * Pulsamos intro y accedemos a esa sesi�n. Como que manejar esos nombres es bastante engorroso, a las sesiones se le puede asignar un nombre m�s amigable con el comando <b>-S</b>
 *
 *		screen -S ServidorPIC
 *
 * Ahora el nombre de la sesi�n es mas manejable
 *
 * \image html ..\..\Documentacion\Imagenes\screen_2.jpg
 *
 * Ahora para acceder a la sesi�n teclearemos
 *
 *		screen -r ServidorPIC
 *
 * Si quisi�ramos arrancar una nueva sesi�n pero sin abrirla usar�amos los comandos <b>-d</b> <b>-m</b>
 *
 *		screen -d -m -S ServidorPIC
 *
 * Si queremos enviarle comandos a la sesi�n sin tener que entrar en ella se utiliza el comando <b>-X</b>
 *
 *		screen -S ServidorPIC -X quit
 *
 * Si lo que queremos es 'matar' la screen, tecleamos
 *
 *		screen -X -S ServidorPic kill
 *
 */


/**
 * \subsection SubSeccionRaspBerry212	2.1.2- SAMBA
 * 
 * Samba permite acceder al sistema de ficheros desde otro sistema operativo, normalmente lo utilizaremos para acceder a la estructura de directorios de Raspberry desde nuestro PC con Windows.
 *
 * Para instalar Samba, lo primero que debemos hacer es instalar los paquetes necesarios en la raspberry
 *
 *		sudo apt-get install samba samba-common-bin
 *
 * Ahora debemos configurar SAMBA para decirle que carpetas compartir, para ello editaremos el fichero  /etc/samba/smb.conf
 *
 *		sudo nano /etc/samba/smb.conf
 *
 * Por defecto Windows crea un grupo de trabajo llamado Workgroup por lo que si estamos utilizando ese grupo lo dejaremos en el fichero de configuraci�n
 *
 * 		# Change this to the workgroup/NT-domain name your Samba server will part of
 *		workgroup = WORKGROUP
 *
 * La l�nea correspondiente a 'wins support' generalmente aparece comentada con una almohadilla #, borramos dicho car�cter para habilitar ese valor y  lo ponemos en <b>yes</b>.
 *
 *		# Windows Internet Name Serving Support Section:
 *		# WINS Support - Tells the NMBD component of Samba to enable its WINS Server
 *		   wins support = yes
 *
 * Buscamos la l�nea donde se conceden permisos sobre ficheros y directorios y los cambiamos para asignarles 0777
 *
 *		# File creation mask is set to 0700 for security reasons. If you want to
 *		# create files with group=rw permissions, set next parameter to 0775.
 *		create mask = 0777

 *		# Directory creation mask is set to 0700 for security reasons. If you want to
 *		#create dirs. with group=rw permissions, set next parameter to 0775.
 *		directory mask = 0777
 *
 * El siguiente paso es buscar un apartado llamado 'Share Definitions' dentro del archivo. Aqu� vamos a crear las carpetas que vamos a compartir en red y a configurarlas seg�n nuestras necesidades. 
 * En nuestro caso vamos a compartir todo el disco de la raspberry, pondremos lo siguiente
 *
 *		[pi]
 *  		comment = Users profiles
 *  		path = /
 *  		browseable = yes
 *  		read only = no
 *
 * Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
 *
 * El �ltimo paso ser� establecer una contrase�a al usuario Pi que nos sera solicitada al entrar en la carpeta de Samba desde la red. Para ello tecleamos:
 *
 *		sudo smbpasswd -a pi
 *
 * Nos pedir� teclear el password y luego volverlo a teclear para confirmar, una vez hecho nos confirmar� que se ha a�adido el usuario pi.
 *
 * Para finalizar, reiniciamos el servicio
 *
 *		sudo systemctl restart smbd
 *		sudo systemctl restart nmbd
 *
 *
 * Si se quiere dar acceso a internet, en el router se deber�n redireccionar los siguientes puertos a la raspberry 
 *
 *			Puertos tcp (135,139 y 445)
 *			Puertos udp (137 y 138)
 *
 * Otros comandos samba
 * Para desinstalar samba
 *
 *		sudo apt-get purge --auto-remove samba
 *
 * Para  analizar smb.conf
 * 
 * 		testparm
 *
 *
 */


/**
* \subsection SubSeccionRaspBerry213	2.1.3.-- LAMP
*
*/


/**
* \subsection SubSeccionRaspBerry2131	2.1.3.1.- Apache
*
* Lo primero que haremos ser� crear y dar permisos al grupo que usa apache por defecto.
*
*		sudo addgroup www-data
*		sudo usermod -a -G www-data www-data
*
* Hacemos un update de los repositorios
*
*		sudo apt-get update
*		sudo apt-get upgrade
*
* Instalamos Apache
*
*		sudo apt-get install apache2
*
* Cuando termine la instalacion, se crea una carpeta por defecto donde estan nuestros 'sitios', ubicada en /var/www; Para cambiar los permisos poner el comando
*
*		sudo chmod -R 775 /var/www
*
* Por defecto, Apache escuchar� en el puerto 80, si queremos cambiar el puerto tendremos que editar el fichero <b>/etc/apache2/ports.conf</b>
*
*		sudo nano /etc/apache2/ports.conf
*
* y modificamos la l�nea <b>Listen 80</b> con el puerto deseado ( Listen 8080 por ejemplo )
*
* Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
*
* Realizamos lo mismo con el fichero <b>/etc/apache2/sites-enabled/ooo-default.conf</b>
*
*		sudo nano /etc/apache2/sites-enabled/000-default.conf
*
* y modificamos la l�nea <b><VirtualHost *:80></b> con el puerto deseado ( VirtualHost *:8080 por ejemplo )
*
* Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
*  
* Con esto queda instalado Apache, ahora solo queda reiniciar el servicio
*
*		sudo /etc/init.d/apache2 restart
*
* Estos son algunos comandos que pueden ser utiles para trabajar con el servidor.
*
*		Inicial el servidor   -> sudo service apache2 start
*		Detener el servidor   -> sudo service apache2 stop
*		Reiniciar el servidor -> sudo service apache2 restart
*		Estado del servidor   -> sudo service apache2 status
*
*
*/


/**
* \subsection SubSeccionRaspBerry2132	2.1.3.2.- PHP
*
* Instalamos PHP
*
*		sudo apt-get install php
*
* Reiniciamos Apache
*
*		sudo /etc/init.d/apache2 restart
*
* Para probar que PHP se instalo correctamente, creamos un archivo <b>phpinfo.php</b> en <b>/var/www/html</b> 
*
*		sudo nano /var/www/html/phpinfo.php
*
* con el siguiente contenido:
*
*		<?php 
*			phpinfo();
*		?>
*
* Ahora, desde un navegador accedemos a la direcci�n/puerto asignados a Apache en la  Raspberry, en nuestor caso, suponiendo que hemos puesto la IP est�tica 192.168.1.20 y el puerto de apache 8080, ser�a:
*
*		http:\\192.168.1.20:8080\phpinfo.php
*
* Si todo se ha realizado correctamente, el servidor nos mostrar� la configuraci�n de PHP
*
*/


/**
* \subsection SubSeccionRaspBerry2133	2.1.3.3.- Mysql
*
* Instalamos Mysql
*
* Una vez hemos instalado Apache y php, procedemos a instalar mysql y phpmyAdmin. Teniendo en cuenta que se van a instalar los dos paquetes, vamos a alterar el orden de poner password a mysql
*
* El primer paso que se realizar� ser� activar nuestra interfaz loopback ya que si no lo hacemos nos dar� un error al instalar MySQL
*
*		sudo ifup lo
*
* El siguiente paso ser� instalar mysql
*
*		sudo apt-get install mysql-server mysql-client
*
*
* Ahora, sin arrancar mysql, dejamos para despu�s de la instalaci�n de phpmyadmin la asignaci�n de password al ususario root de mysql.
*
*
* Estos son algunos comandos que pueden ser utiles para trabajar con el servidor.
*
*		Inicial el servidor   -> sudo service mysql start
*		Detener el servidor   -> sudo service mysql stop
*		Reiniciar el servidor -> sudo service mysql restart
*
*/


/**
* \subsection SubSeccionRaspBerry2134	2.1.3.4.- Phpmyadmin
*
* Instalamos phpmyadmin
*
*		sudo apt-get install phpmyadmin
*
* Se instalara el programa y nos preguntara que servidor tenemos instalado, seleccionaremos apache2 ( con un espacio ) y con tabulador vamos a <b><Ok></b> y pulsamos intro>
*
* \image html ..\..\Documentacion\Imagenes\phpmyadmin_1.JPG
*
* Luego nos presentara la siguiente pantalla a la que debemos responder <b>Yes</b>
*
* \image html ..\..\Documentacion\Imagenes\phpmyadmin_2.JPG
*
* Por �ltimo, nos preguntara el password utilizado para mysql y lo dejaremos en blanco pulsando <intro>
*
* \image html ..\..\Documentacion\Imagenes\phpmyadmin_3.JPG
*
* Una vez instalado todo el paquete editaremos el fichero <b>/etc/apache2/apache2.conf</b>
*
*		sudo nano /etc/apache2/apache2.conf
*
*  Le a�adimos como �ltima l�nea el siguiente texto
*
*		Include /etc/phpmyadmin/apache.conf
*
* Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
*
* Editamos el fichero <b>/etc/php/7.0/apache2/php.ini</b>
*
*		sudo nano /etc/php/7.0/apache2/php.ini
*
* Descomentamos la linea 
*
*		extension=msql.so
*
* que se encuentra en la seccion  <b>Dynamics Extensions</b>
*
* Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
*
* @warning El path donde se encuentra php.ini puede cambiar en funcion de la versi�n de php instalada. El path detallado es para PHP 7,  para PHP 5 el path ser�a /etc/php5/apache2/php.ini
*
* Ponemos en marcha el servidor mysql con el comando
*
*		sudo service mysql restart
*
* Reiniciamos Apache
*
*		sudo service apache2 restart
*
* Ya podemos acceder a PhpMyAdmin en la direcci�n de la Raspberry. En este caso seria
*
*		http://192.168.1.20:8080/phpmyadmin/		
*
*/


/**
* \subsection SubSeccionRaspBerry2135	2.1.3.5.- Asignacion password a usuario root de mysql
*
* Una vez instalado mysql y phpmyadmin, se debe asignar password al ususario root de mysql, para ello, se debe entrar en el servidor sin clave
*
* 		sudo mysql -u root
*
* A la respuesta para introducir comandos en mysql se le debe dar el siguiente comando para establecer contrase�a al administrador
*
*		grant all privileges on *.* to root@localhost identified by 'XXXXXXX' with grant option;
*
* Siendo XXXXXXX el password que se desea para el usuario root
*
* Luego teclear
*
*		flush privileges;
*
* Por �ltimo teclear 'quit' para salir del servidor mysql 
* 
* Para verificar que la instalaci�n quedo correcta, accedemos a mysql de la siguiete forma
*
*		sudo mysql -uroot -p
*
* Nos pedir� la contrase�a y nos dar� acceso a la consola Mysql. con <b><Ctrl> <Z></b> saldremos de ella
*
*/




/**
* \subsection SubSeccionRaspBerry214	2.1.4.- Java
*
* Instalamos Java
*
* Se deber� instalar la ultima versi�n, a la hora de realizar este documento, la ultima version era oracle-java8-jdk
*
*		sudo apt-get update && sudo apt-get install oracle-java8-jdk
*
* Nos aseguramos de que se ha instalado viendo la versi�n
*
*		sudo java -version
*
* Comprobaremos que la versi�n es como m�nimo la <b>1.8.0_65</b>
*/

//-------------------------------------------------------
//Pagina2  Software Basico 
//-------------------------------------------------------
//-------------------------------------------------------
//Pagina2 Seccion 2 
//		2.2 Instalacion Software especifico
//			2.2.1 ServerPic
//-------------------------------------------------------
/**
 * \subsection SubSeccionRaspBerry22	2.2.- Instalacion de software especifico
 *
 */
/**
 * \subsection SubSeccionRaspBerry221	2.2.1- ServerPIC
 *
 * ServerPIC es un servidor que permite la conexion de varios clientes mediante 'Sockets'. Los clientes, una vez conectados al servidor tienen distintas funcionalidaes que, entre otras, permite el envio de mensajes entre clientes.
 *
 * Las funcionalidades de los clientes habilitadas para ServerPic en este proyecto son:
 *
 *		mensaje-:-<Cliente>-:-Texto Mensaje<intro> .- Envia 'Texto Mensaje' al cliente conectado con nombre 'Cliente'  
 *                                                       		El cliente recibe el siguente mensaje <Remitente>-:-Texto Mensaje. 
 *                                                       		Si <Cliente> no esta en ese momento conectado se guarda el mensaje hasta que se conecte
 *		hora<intro>                                .- Recibe la hora del servidor
 *		latido<intro>                              .- Recibe el texto 'Ok' del servidor
 *		ping-:-<Cliente>                           .- Recibe 'Pong' desde 'Cliente' si este esta conectado. 			 
 *		load-:-<NombreVariable>-:-valor            .- Guarda 'Valor' en la variable 'NombreVariable' para el cliente que lo solicita
 *		save-:-<NombreVaribable>                   .- El servidor envia el 'valor' alamcenado para este cliente en 'NombreVariable'
 *
 * Para instalar ServerPic, se ha creado un paquete espec�fico que se encargar� de bajar el programa, instalarlo como servicio y crear las tablas necesarias en mysql
 *
 * 
 * Para bajar el paquete ir al directorio /home/pi y teclear 
 *
 *		wget http://lleida.gotdns.com:2080/ServerPic/ServerPic.deb
 *
 * Descomprimimos el paquete
 *
 *		sudo dpkg -i ServerPic.deb
 *
 * Por defecto el servidor escuchar� en el puerto 2001, podemos cambiar ese puerto editando dos l�neas del fichero /usr/local/bin/serverpic.sh con 
 *
 *		sudo nano /usr/local/bin/serverpic.sh
 *
 * Cambiar el puerto 2001 por el deseado en las dos l�neas donde aparece.
 *
 * Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
 *
 * Ya tenemos el programa y el servicio creado, ahora crearemos las tablas en mysql, se nos pedir� el password del usuario root de mysql 
 *
 *		mysql -u root  -p < ServerPic.sql
 *
 * Con el paso anterior se crean las tablas necesarias para ServerPic y un usuario para acceder a esas tablas. El usuario creado para acceso del servidor a mysql es <b>jusaba</b> y el password <b>JuSaBa2408</b>
 *
 * @warning Esta versi�n de ServerPic no permite cambiar el usuario y password de acceso a mysql
 *
 * Por �ltimo vamos a activar el servicio y lo vamos a arrancar
 *
 *		sudo systemctl enable serverpic.service
 *		sudo systemctl daemon-reload
 *		sudo systemctl start serverpic.service
 *
 * Para ver la actividad del servidor se crea un fichero log donde se vuelca la actividad. El fichero se encuentra en /home/pi/ServerPic y su nombre es serverpic.log
 *
 * Para visualizar el fichero, en ese directorio teclear
 *
 *		tail -f serverpic.log  
 *
 * Si no queremos escribir tanto cada vez que queremos visualizar la actividad del servidor podemos crear un alias 
 *
 *		cd /home/pi
 *		touch ~/.bash_aliases
 *		sudo nano ~/.bash_aliases
 *
 * Escribir en ese fichero la l�nea
 *
 *		alias serverpiclog='tail -f /home/pi/ServerPic/serverpic.log'
 *
 * Guardamos los cambios pulsando <b><Ctrl>O</b> y salimos con <b><Ctrl>X</b> 
 *
 * Ahora, siempre que el usuario pi quiera ver la actividad, tan solo se debe teclear
 *
 *		serverpiclog
 *
 * Cuando queramos abandonar la visualizacion, pulsar <b><Ctrl>Z</b>
 *
 * Una vez que hemos preparado la Raspberry y con ServerPic funcionando, es el momento de volver al \ref Indice Principal para continuar con los transductores y actuadores.
 */
//-------------------------------------------------------
//Pagina2 Instalacion Software  
//-------------------------------------------------------
 /**
 * \page PaginaRaspBerry2 Instalacion Software
 * 
 */
/*! \page  page1  Documentacion
  	\tableofcontents
*
* Documentaci�n del m�dulo 
 		\section Sec2 Enlaces de interes
 			<a href="https://polaridad.es/esp8266-modulo-wifi-elegir-caracteristicas/">Que modelo ESP8266 elegir</a> del sitio <b>Polaridad.es</b>
			<a href="http://esp8266.github.io/Arduino/versions/2.3.0/">ESP8266 Arduino Core</a>
		\section Sec3 Documentacion Tecnica
*/









