 /**
******************************************************
* @file PagModuloBasico.h
* @brief Documentacion del m�dulo B�sico
* @author FabLab Lleida
* @author Jose Martinez Molero
* @author Julian Salas Bartolome
* @version 1.0
* @date Abril 2018
*
*
*******************************************************/

 /**
 * \section Basico1 3.1.- M�dulo B�sico
 * Este apartado describe el software necesario para integrar un m�dulo basado en ESP8266 en un sistema controlado por ServerPic. A partir de este software se dise�ar�n m�dulos espec�ficos para 
 * cada utilizaci�n. 
 *
 *
 * Para que se conecte al sistema, en primer lugar el m�dulo debe conectarse a la SSID, una vez conectado, se debe 
 * conectar al servidor con un nombre de usuario que ser� �nico para cada dispositivo.
 *
 * Para conectarse a la SSID es necesario que el m�dulo conozca el nombre de la SSID y el password, para eso, cuando el m�dulo
 * se inicia por primera vez, se pone en modo AP Y crea la red wifi <b>Jusaba </b> con password <b>24081960</b>. Conect�ndose a
 * esa red con un terminal y accediedo con un navegador a la direcci�n <b>192.168.4.1:80</b> se nos presenta un formulario de configuraci�n
 * en el que se solicitan los datos de la SSID asi como los datos de ususario en el servidor. Una vez enviado el formulario
 * el m�dulo graba esos datos en EEprom de forma que la siguiente inicializaci�n, el m�dulo se conectar� ya a la SSID y
 * posteriormente al servidor.
 *
 * Una vez conectado al servidor, el m�dulo quedar� a la escuha y ser� capaz de recibir ordenes desde el servidor y enviar informaci�n a otros m�dulos a traves de el.
 *
 * Todo esto se detallar� cuando se profundice en la descripci�n de cada uno de los m�dulos espec�ficos.
 * 
 * \subsection Basico11	3.1.1. - Hardware 
 * El sistema esta basado en m�dulos ESP8266. La preparaci�n de estos m�dulos para ser utilizados en nuestro sistema se detalla en \ref SecPreparacionESP82661
 * \subsection Basico12	3.1.2.- Software 
 *
 * Los m�dulos de software en ESP8266 son los siguientes:
 *
 * \subsection Basico121	3.1.2.1.- Software com�n  
 *
 *	- Configuracion.h 	Funciones para configurar el m�dulo. Crea una estructura de datos de configuraci�n ( DataConfig ) y las funciones para almacenrla y recuperarla de la EEPROM
 *	- Eprom.h 		Funciones para operar con la EEPROM. Permiten guardar/recuperar configuraci�nes del m�dulo a/de la EEPROM   
 *	- Global.h 		Funciones y parametros generales de la aplicacion que por definici�n no pueden ser ubicadas en m�dulos de software espec�ficos. 	
 *	- ModoAP.h 		Funciones que ponen el m�dulo en modo AP, env�a la p�gina de configuraci�n, una ver rellenada, la recibe y extrae los datos de configuraci�n para grabarla en Eprom con los paquetes anteriores.
 *	- ModoOTA.h 		Funciones que ponen el m�dulo en modo OTA para grabaci�n del modulo de forma inalambrica
 *	- ModoSTA.h 		Funciones que ponen el m�dulo en modo STA y lo conecta a la red correspondiente a la configuraci�n guardada en EEPROM
 *	- Servidor.h 		Funciones que conectan el m�dulo a ServerPic y gestiona la comunicaci�n desde y hacia  ServerPic mediante una estructura de datos definida en este modulo ( Telegrama )	
 *
 * \subsection Basico122	3.1.2.2.- Software espec�fico
 *
 *	- Basico.ino  			Programa principal. Podra tener el nombre que se considere m�s adecuado a cada dise�o
 *	- ServerPic.h 			Fichero de cabeceras de Basico.ino. En este fichero se declaran las condicionantes de compilacion ( debuger, homekit, .... )
 *                          y los datos de software y hardware del modulo 
 *	- IO.h 				Fichero con asignacion de los pines utilizados	en funci�n del modelo ESP empleado	
 *
 * En el siguiente gr�fico se puede ver el diagrama de funcionamiento del m�dulo desde que arranca hasta que se conecta a ServerPic para iteractuar con el
 *
 *
 * \image html Documentacion\Imagenes\Diagrama1.png
 *
 * 
 * Lo m�s apropiado para los m�dulos de software comunes para todos los dispositivos es ubicarlos en la carpeta de librerias de arduino.
 *
 *		C:\Users\<Usuario>\Documents\Arduino\libraries\ServerPic
 *
 * El resto de m�dulos, los espec�ficos, se ubicar�n en la carpeta donde se desarrolle la aplicaci�n del dispositivo.
 * 
 * El bloque denominado 'Iteractua con ServerPic' sera exclusivo para cada dispositivo y en el se desarrolar� el comportamiento del mismo a las distintas ordenes que pueda recibir, ese blouqe
 * es el que se debe dise�ar para cada aplicaci�n del dispositivo( rele wifi, interruptos wifi, sensor de temepratura wifi, .... ).
 *
 * En otro apartado se desarrollar�n esos dispositivos y se detallar� para cada uno su bloque 'Iteractua con ServerPic'
 *
 * Aunque de cara al usuario, la configuraci�n del m�dulo es un proceso autom�tico que se realiza mediante una p�gina de configuraci�n, de cara al desarrollador hay que tener en cuenta un par de datos para la configuraci�n
 *
 * En modo AP, la red que crea ( nombrada anteriormente como Jusaba con password 24081960 ) se puede configurar en el fichero ModoAP.h en las l�neas
 *
 *
 * \code
 				#define ssidAP  "Jusaba"
 				#define passwordAP  "24081960"
 * \endcode
 *
 * Como se dec�a anteriormente, para personalizar cada m�dulo, es decir para cada aplicaci�n espec�fica debe dise�arse un bloque 'iteractua con ServerPic', el programa principal ( Basico.ino ) tendr� la siguiente estructura
 *
 * \image html Documentacion\Imagenes\EstructuraIno.png
 * 
 * <b>Includes</b> El programa incluye tan solo dos ficheros aunque, uno de ellos, es el que verdaderamente tiene todos los includes necesarios de la aplicaci�n.
 * 
 * \code
 		#include "ServerPic.h"		Fichero general de includes y otras configuraciones
 		#include IO.h 				Se carga el fichero con la configuraci�n de E/S de los distintos modelos ESP8266 		
 * \endcode

 * El fichero que en realidad tiene todos los includes es <b>ServerPic.h</b>.  
 *
 * \code
	//----------------------------------------------
	//Includes
	//----------------------------------------------
	#include <ESP8266WiFi.h>
	#include <WiFiClient.h> 
	#include <ESP8266WebServer.h>

	#include "Global.h"
	#include "Configuracion.h"
	#include "Eprom.h"
	#include "ModoAP.h"
	#include "ModoSTA.h"
	#include "Servidor.h"

	#include <WiFiUdp.h>
	#include <ArduinoOTA.h>


	//----------------------------
	//CARACTERISTICAS DISPOSITIVO
	//----------------------------
	#define ESP11					//Modelo de ESP8266
	#define Ino "Outlet"				//Nombre del programa principal
	#define VIno "1.0"				//Version del programa principal
	#define Placa "IOT ESP11 1.0"			//Placa utilizada

	//---------------------------------
	//CARACTERISTICAS DE LA COMPILACION
	//---------------------------------
	#define Compiler "Stino";				//Compilador
	#define VCore "1.4.0";				//Versi�n del Core Arduino

	#include "IO.h";


 	 	//----------------------------------------------
 		//Debug
 		//----------------------------------------------
 		#define debug 					//Permite los mensajes Debug, se eliminar�n en la versi�n de producci�n
 
 	 	//----------------------------------------------
 		//Home Kit de Apple
 		//----------------------------------------------
 		#define HomeKit 				//El dispositivo se integrar� en HomeKit, 
 
 
 	 	//----------------------------------------------
 		//Teimpo de Test de conexion
 		//----------------------------------------------
 		#define TiempoTest	10000			//Tiempo en milisegundos para Test de conexion a servidor
 
 
		//Variables donde se almacenan los datos definidos anteriormente para pasarlos a Serverpic.h
		//para mandar la informaci�n del Hardware y Software utilizados
		//En la libreria ServerPic.h estan definidos como datos externos y se utilizan en la funcion
		//Mensaje () para responder al comando generico #Info.
		//Con ese comando, el dispositivo mandara toda esta informaci�n al cliente que se lo pida
		// ESTOS DATOS NO SON PARA MODIFICAR POR USUARIO
		//----------------------------
		//Datos del programa principal
		//----------------------------
		String VMain = VIno;
		String Main = Ino; 
		//----------------------------
		//Datos de compilaci�n
		//----------------------------	
		String Compilador = Compiler;
		String VersionCore = VCore;
		//----------------------------
		//Datos de Hardware
		//----------------------------	
		String ModeloESP = Modelo;
		String Board = Placa;

 * \endcode
 * 
 * Adem�s de los <b>#includes</b>, <b>ServerPic.h</b> contiene otros apartados
 *
 * En primer lugar se definen datos t�cnicos del software y hardware para poder acceder a ellos posteriormente
 
 * Luego vienen los condicionantes de la compilaci�n
 
 * La l�nea #define Debug se utiliza para la fase de desarrollo, el esp8266 manda mensjaes de Debug al monitor. En fase de producci�n se eliminar� la l�nea para no enviar informacion pr el puerto serie y poder utilizar sus pins
 *
 * La l�nea #define HomeKit se utiliza por si se desea que el dispositivo forme parte del Home Kit de Apple
 *
 * Otro apartado define cada cuantos milisegundo se comprueba la conexi�n del m�dulo al  servidor. De forma c�clica, se comprueba la conexi�n con el servidor, si por cualquier causa se ha interrumpido esa conexi�n
 * el modulo comprobar� si es probleMa de conexi�n a SSID o al servidor e intentara restablecerla. Con este proceso se evita que alg�n m�dulo quede 'descolagado' del sistema de forma fortuita.
 *
 * Evidentemente, existe la posibilidad de que un m�dulo no comprueba la conexi�n, esto ser�i util para m�dulos que actuen de forma circusntancial y solo se conecten al servidor cuando se produce alg�n evento, en ese caso
 * se asignar� un tiempo <b>0</b>, el m�dulo entender� que no debe probar la conexi�n.  
 *
 * En el fichero IO.h se definen los pin utilizados en la aplicaci�n en fucnci�n del modelo ESP8266 utilizado
 * 
 *\code
	#ifdef ESP01
		#define Modelo "ESP01"
		int PinReset = 0;														
	#endif
	#ifdef ESP11
		#define Modelo "ESP11"
		int PinReset = 0;														
	#endif		
	#ifdef ESP12
		#define Modelo "ESP12"
		int PinReset = 0;														
	#endif	
 *\endcode
 *
 *
 * <b>Declaracion de variables</b> Se declaran variables globales de la aplicaci�n. Algunas de estas variables son requeridas posterirmente por las librerias por lo que son de obligada declaraci�n
 *
 *\code
 	ESP8266WebServer server(80);			//Definimos el objeto Servidor para utilizarlo en Modo AP
 	WiFiClient Cliente;				//Definimos el objeto Cliente para utilizarlo en Servidor
 	Telegrama oMensaje;				//Estructura mensaje donde se almacenaran los mensajes recibidos del servidor
 
 	String cDispositivo = String(' ');		//Variable donde se deja el nombre del dsipositivo. Se utiliza para comunicar con HomeKit
 	String cSalida;					//Variable donde se deja el estado ( String ) para mandar en mensaje a ServerPic
 	boolean lEstado = 0;				//Variable donde se deja el estado del dispositivo para reponer el estado en caso de reconexion
 	boolean lConexionPerdida = 0;			//Flag de conexion perdida, se pone a 1 cuando se pierde la conexion. 
 
 	long nMiliSegundosTest = 0;			//Variable utilizada para temporizar el Test de conexion del modulo a ServerPic
 
 *\endcode

 * <b>Setup</b> En el bloque setup se inicializan la EEPROM, se establecen entradas/salidas y se realiza lza conexi�n con el servidor ( Si el m�dulo es 'virgen' se pone en modo AP ) 
 *
 *\code
 	void setup() {
 
 		#ifdef Debug										//Usamos el puerto serie solo para debugar	
 			Serial.begin(9600);								//Si no debugamos quedan libres los pines Tx, Rx para set urilizados
 		#endif
 		EEPROM.begin(128);									//Reservamos zona de EEPROM
 		pinMode(PinReset, INPUT);								//Configuramos el pin de reset como entrada
 		if ( LeeByteEprom ( Flagconfiguraci�n ) == 0 )						//Comprobamos si el Flag de configuraci�n esta a 0
 		{											// y si esta
 			ModoAP();									//Lo ponemos en modo AP
 		}else{											//Si no esta
 			if ( ClienteSTA() )								//Lo poenmos en modo STA y nos conectamos a la SSID
 			{										//Si ha conseguido conectarse a ls SSID en modo STA
 				if ( ClienteServerPic () )						//Intentamos conectar a ServerPic
 				{
 					#ifdef Debug
 						Serial.println(" ");
 						Serial.println("Conectado al servidor");
 					#endif    
  				#ifdef HomeKit
						DataConfig aCfg = EpromToConfiguracion ();		//Leemos la configuracin de la EEprom
						char USUARIO[1+aCfg.Usuario.length()];			//Almacenamos en el array USUARIO el nombre de usuario 	
						(aCfg.Usuario).toCharArray(USUARIO, 1+1+aCfg.Usuario.length());           
						cDispositivo = USUARIO;
					#endif

 				}
 			}	
 		}
 	}
 *\endcode
 *
 * <b>Loop</b>  Este bloque  es el bucle del programa principal, se testea el boton de reset, se testea la conexi�n, se comprueba si se recibe una orden y si es as�, 
 * se analiza y se actua en consecuencia por �ltimo, si se ha recibido una orden, se actualiza por WebSocket las posibles paginas html abiertas y se
 * informa a HomeKit 
 *
 *\code
 	void loop() {

 		------------------
 		Comprobacion Reset
 		------------------

 		TestBtnReset (PinReset);                    	                	//Comprobamos si esta pulsado el boton Reset

 		---------------------
 		Comprobacion Conexion
 		---------------------
 
 		if ( TiempoTest > 0 )							//Si se ha definido tiempo test conexion
 		{
 			if ( millis() > ( nMiliSegundosTest + TiempoTest ) )		//Comprobamos si existe conexion  
 			{ 
 				nMiliSegundosTest = millis();
 				if ( !TestConexion() )					//Si se ha perdido la conexion
				{
					lConexionPerdida = 1;				//Ponemos el flag de perdida conexion a 1
					//------------------------------------------------------------------------
					//Aqui pondremos las acciones a ejecutar si se pierde la conexion
					//Por ejemplo, se puede guardar el estado del sipositivo telecontrolado
					//y se puede poner en off hasta que se recupera la conexion
					//------------------------------------------------------------------------
				}else{							//Si existe conexion
					if ( lConexionPerdida )				//Comprobamos si es una reconexion ( por perdida anterior )
					{						//Si lo es
						lConexionPerdida = 0;			//Reseteamos flag de reconexion
						//------------------------------------------------------------------------
						//Aqui pondremos las acciones a ejecutar cuando se recupera la conexion
						//Por ejemplo, podriamos restaurar el dispositivo al estado anterior a la
						//perdida de conexion
						//------------------------------------------------------------------------

					}	
				}
			}
		}	

 		------------------
 		Analisis Comandos
 		------------------

 		oMensaje = Mensaje ();                          	         	//Iteractuamos,comprobamos si se ha recibido alg�n mensaje
		if ( oMensaje.lRxMensaje)                           	      	 	//Si se ha recibido ( oMensaje.lRsMensaje = 1)
		{
			#ifdef Debug				
				Serial.println(oMensaje.Remitente);						
				Serial.println(oMensaje.Mensaje);
			#endif	
			//-------------------------------------------------------------------------------------------	 
			//En este punto empieza el bloque de programa particular del dispositivo segun la utilizacion	
			//Se ha de tener en cuenta el refresco de wdt en caso de que el proceso sea demasiado largo
			//En este Bloque se deja en cSalida el texto que se desea enviar a WebSocket y a HomeKit
			//-------------------------------------------------------------------------------------------	 

 			-----------------------
 			Actualizaci�n WebSocket
 			-----------------------

			if ( cSalida != String(' ') )					//Si hay texto en cSalida
			{	
				EnviaMensajeWebSocket(cSalida);				//Actualizamos las p�ginas html conectadas
			}

	 		--------------------
 			Notificacion HomeKit
 			--------------------
	 		
			#ifdef HomeKit							//Si esta definido HomeKit, enviamos informaci�n al dispositivo espejo siempre que la orden npo venga precisamente de ese dispositivo ( HomeKit )
				if ( cSalida != String(' ') && oMensaje.Remitente != ( cDispositivo + String("_") ) )
				{
					oMensaje.Destinatario = cDispositivo + String("_");
					oMensaje.Mensaje = cSalida;
					EnviaMensaje(oMensaje);
				}
			#endif		
			cSalida = String(' ');
		}
		wdt_reset();                                                		//Refrescamos WDT
	}
 *\endcode
 *
 *
 * \subsection Basico123	3.1.2.3.- Ordenes de servicio
 *  
 * Como se  coment� en \ref TransductoresActuadores, la comunicaci�n entre m�dulos se realiza mediante mensajes gestionados por el servidor. En esos mensajes, en el campo <b>Texto Mensaje</b>, se transmite la orden 
 * de la acci�n que se requiere del m�dulo receptor. Las ordenes, son particulares para cada dispositivo, una sonda de temperatura podr� tener la orden de que mida la temperatura y la envie a un termostato pero,
 * una bombilla tendr� la orden de encendido o apagado.
 *
 * Cada dispositivo pues, en el bloque tantas veces menciondado de <b>iteractua con ServerPic</b> decodificar� la orden y la ejecutar�.
 * 
 * A pesar de que cada dispositivo tiene sus propias ordenes, existen ordenes que no est�n relacionados directamente con el funcionamiento del aparato telecontrolado, por ejemplo,  resetear un modulo, hacdrle un ping, 
 * preguntar la Ip de un m�dulo, ..... En f�n, existen una serie de ordenes, a las que vamos a denominar <b>Ordenes de servicio</b>, que ser�n comunes a todos los dispositivos.
 *
 * Por el hecho de ser comunes, lo suyo es incluirlos en los bloques de Software Com�n. Lo coherente es incluir estas ordenes en la librer�a Servidor.cpp. Cuando se comprueba si se ha recibido un mensaje con la funci�n Mensaje(), 
 * antes de devolver el mensaje recibido, la funci�n, analizar� si es una orden se servicio, si es as�, ejecutar� la orden y se saldr� de la funci�n como si no hubiera recibido una orden especifica de dispositivo.
 *
 * Para simplificar el an�lisis se ha decidido que todas las ordenes de servicio empiecen con el caracter <b>#</b>
 *
 * La lista actual de ordenes de servicio es la siguiente:
 *
 *       #ping.-                 Devuelve pong al Remitente que le solicito ping
 *       #chipid.-               Devuelve el id del chip empleado en el modulo
 *       #reset.-                Resetea el modulo
 *       #factory.-              Borra la flash y deja el modulo 'virgen'
 *       #nombressid.-           Cambia en EEPROM el nombre de la SSID actual por la que se le envia como parametro en esta orden
 *       #passwordssid.-         Cambia en EEPROM el password de la SSID actual por el que se le pasa como parametro en esta orden
 *       #usuarioservidor.-      Cambia en EEPROM el nombre de usuario del servidor actual por el que se le pasa como parametro en esta orden
 *       #Info.-                 Devuelve una cadena con la informaci�n del modulo ( ChipId, IP. y la informaci�n de #Version)
 *       #IP.-                   Devuelve la IP del modulo
 *       #OTA.-                  Pone el m�dulo en modo OTA
 * 
 * 
 */

//-------------------------------------------------------
//Pagina3 Transductores y Actuadores
//-------------------------------------------------------
/**
 * \page Basico1 3.1.- M�dulo B�sico
 *
 */