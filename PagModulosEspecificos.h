/**
******************************************************
* @file PagModulosEspecificos.h
* @brief Documentacion de los m�dulos espec�ficos
* @author FabLab Lleida
* @author Jose Martinez Molero
* @author Julian Salas Bartolome
* @version 1.0
* @date Mayo 2018
*
*
*******************************************************/

/**
* \section SeccionEspecificos 3.2- M�dulos Espec�ficos
*
* Despu�s de la descripci�n \ref Basico1, es la hora de empezar con los m�dulos espec�ficos. Estos m�dulos son lo que verdaderamente se van a utilizar y, para cada utilizaci�n, se dise�ar� un software espec�fico.
*
* Lo m�s sencillo ser�a proporcionar una serie de modelos espec�ficos y ponerlos en marcha, este sistema es un sistema abierto,  cualquier persona con conocimeintos elementales de la plataforma Arduino debe ser 
* capaz de dise�ar sus propios m�dulos para cubrir sus necesidades pero , para ello, son necesarias unas pautas generales.
*
* Pongamos foco en una parte del bloque loop del m�dulo b�sico, teniamos la siguiente estructura
*
*\code
	void loop() {
		
 		------------------
 		Comprobacion Reset
 		------------------
 		.
 		---------------------
 		Comprobacion Conexion
 		---------------------
 		.
 		.
 		------------------
 		Analisis Comandos
 		------------------

			oMensaje = Mensaje ();						//Iteractuamos con ServerPic,comprobamos si se ha recibido alg�n mensaje
			if ( oMensaje.lRxMensaje)					//Si se ha recibido ( oMensaje.lRsMensaje = 1)
			{
				#ifdef Debug				
					Serial.println(oMensaje.Remitente);						
					Serial.println(oMensaje.Mensaje);
				#endif	
				//-------------------------------------------------------------------------------------------	 
				//En este punto empieza el bloque de programa particular del dispositivo segun la utilizacion	
				//Se ha de tener en cuenta el refresco de wdt en caso de que el proceso sea demasiado largo
				//-------------------------------------------------------------------------------------------	 

 			-----------------------
 			Actualizaci�n WebSocket
 			-----------------------
		
				if ( cSalida != String(' ') )				//Si hay texto en cSalida
				{	
					EnviaMensajeWebSocket(cSalida);			//Actualizamos las p�ginas html conectadas
				}

	 		--------------------
 			Notificacion HomeKit
 			--------------------
	 		
				#ifdef HomeKit						//Si esta definido HomeKit, enviamos informaci�n al dispositivo espejo siempre que la orden no proceda de este dispositivo ( HomeKit )
					if ( cSalida != String(' ') && oMensaje.Remitente != ( cDispositivo + String("_") ) )
					{
						oMensaje.Destinatario = cDispositivo + String("_");
						oMensaje.Mensaje = cSalida;
						EnviaMensaje(oMensaje);
					}
				#endif		
				cSalida = String(' ');
			}	 	
			wdt_reset();							//Refrescamos WDT
	}
*\endcode
*
* En la l�nea 
*
*\code
*	oMensaje = Mensaje();
*\endcode 
* 
* Lo que hacemos es llamar a una funci�n que comprueba si nuestro m�dulo ha recibido un mensaje del servidor. Si se ha recibido y no es un 'Comando de Servicio', la funci�n retornara una estructura de datos Telegrama con
* el <b>Remitente</b>, el <b>Texto Mensaje</b> y el flag <b>lRxMensaje</b> a '1'
*
* Justo despu�s de la llamada a esa funci�n, si se ha recibido un mensaje ( lRxMensaje = 1 ), se tendr� que analizar el campo <b>Texto Mensaje</b> para comprobar si contiene alguno de los comandos del m�dulo, por ejemplo, imaginemos que
* de nuestro m�dulo 'cuelga' una bombilla conectada mediante un rel� que se gobierna desde el Pin denominado 'PinRele', escribir�amos el siguiente c�digo:
*
*\code
	void loop() {
		
 		------------------
 		Comprobacion Reset
 		------------------
 		.
 		---------------------
 		Comprobacion Conexion
 		---------------------
 		.
 		.
 		------------------
 		Analisis Comandos
 		------------------

			oMensaje = Mensaje ();						//Iteractuamos con ServerPic,comprobamos si se ha recibido alg�n mensaje
			if ( oMensaje.lRxMensaje)					//Si se ha recibido ( oMensaje.lRsMensaje = 1)
			{
				#ifdef Debug				
					Serial.println(oMensaje.Remitente);						
					Serial.println(oMensaje.Mensaje);
				#endif	
				if (oMensaje.Mensaje == "On")				//Si se recibe 'On', se pone PinRele a '1'
				{	
					digitalWrite(PinRele, HIGH);	
					cSalida = "On";	
				}
				if (oMensaje.Mensaje == "Off")				//Si se recibe 'Off', se pone PinRele a '0'
				{		
					digitalWrite(PinRele, LOW);
					cSalida = "Off";		
				}

 			-----------------------
 			Actualizaci�n WebSocket
 			-----------------------

				if ( cSalida != String(' ') )				//Si hay texto en cSalida
				{	
					EnviaMensajeWebSocket(cSalida);			//Actualizamos las p�ginas html conectadas
				}

	 		--------------------
 			Notificacion HomeKit
 			--------------------
	 		
				#ifdef HomeKit						//Si esta definido HomeKit, enviamos informaci�n al dispositivo espejo
					if ( cSalida != String(' ') )
					{
						oMensaje.Destinatario = cDispositivo + String("_");
						oMensaje.Mensaje = cSalida;
						EnviaMensaje(oMensaje);
					}
				#endif		

				cSalida = String(' ');			
			}
			wdt_reset();							//Refrescamos WDT
	}    

*\endcode
* 
* Como se puede observar, si se recibe un mensaje  ( lRxMensaje = 1 ), se comprueba si el texto recibido es 'On' o es 'Off' y en funci�n de ese texto, se establecer� el estado del rel�.
* 
* Para completar el c�digo, evidentemente, ser�a necesario declarar 'PinRele' y establecerlo como salida en el bloque setup.
*
* Si suponemos que nuestro m�dulo se conect� a ServerPic con el nombre <b>Bombilla</b>, para encender la bompilla, desde un terminal putty nos conectariamos al servidor con cualquier nombre y escribir�amos
*
*		mensaje-:-Bombilla-:-On
*
* La bombilla se encnder�a y, evidentemente, con el texto 'Off' se conseguir�a el efecto contrario.
*
* Es posible que pueda interesar que nuestro m�dulo mande un mensaje con el estado de la bombilla al remitente que lo requiera, para eso, podr�amos a�adir un nuevo comando 'Get' que  lo que har� ser� precisamente eso, 
* mandar al remitente el estado de la bombilla, ahora el c�digo quedar�a de la siguiente forma 
*
*\code
	void loop() {
		
 		------------------
 		Comprobacion Reset
 		------------------
 		.
 		---------------------
 		Comprobacion Conexion
 		---------------------
 		.
 		.
 		------------------
 		Analisis Comandos
 		------------------

			oMensaje = Mensaje ();						//Iteractuamos con ServerPic,comprobamos si se ha recibido alg�n mensaje
			if ( oMensaje.lRxMensaje)					//Si se ha recibido ( oMensaje.lRsMensaje = 1)
			{
				#ifdef Debug				
					Serial.println(oMensaje.Remitente);						
					Serial.println(oMensaje.Mensaje);
				#endif	
				if (oMensaje.Mensaje == "On")				//Si se recibe 'On', se pone PinRele a '1'
				{	
					digitalWrite(PinRele, HIGH);	
					cSalida = "On";	
				}
				if (oMensaje.Mensaje == "Off")				//Si se recibe 'Off', se pone PinRele a '0'
				{		
					digitalWrite(PinRele, LOW);
					cSalida = "Off";		
				}
				if (oMensaje.Mensaje == "Get")				//Si se recibe 'Get', se devuelve el estado de PinRele al remitente
				{	
					if ( digitalRead(PinRele) == 1)
					{
						cSalida = "On";
					}else{
						cSalida = "Off";
					}
					oMensaje.Mensaje = cSalida;
					oMensaje.Destinatario = oMensaje.Remitente;
					EnviaMensaje(oMensaje);	
				}	
 			-----------------------
 			Actualizaci�n WebSocket
 			-----------------------

				if ( cSalida != String(' ') )				//Si hay texto en cSalida
				{	
					EnviaMensajeWebSocket(cSalida);			//Actualizamos las p�ginas html conectadas
				}

	 		--------------------
 			Notificacion HomeKit
 			--------------------
	 		
				#ifdef HomeKit						//Si esta definido HomeKit, enviamos informaci�n al dispositivo espejo
					if ( cSalida != String(' ') )
					{
						oMensaje.Destinatario = cDispositivo + String("_");
						oMensaje.Mensaje = cSalida;
						EnviaMensaje(oMensaje);
					}
				#endif		

				cSalida = String(' ');			
			}
			wdt_reset();							//Refrescamos WDT
	}    

*\endcode
*
*
* En ese c�digo, lo �nico que se debe destacar es la forma de enviar un mensaje al remitente
*
*\code
				oMensaje.Mensaje = cSalida;
				oMensaje.Destinatario = oMensaje.Remitente;
				EnviaMensaje(oMensaje);	
*\endcode
* 
* Rellenamos los campos <b>Mensaje</b> y <b>Destinatario</b> y enviamos el mensaje ( En este caso, como destinatario ponemos al remitente, pero podr�a ser otro destinatario e incluso varios destinatarios ).
*
* Con esta exposici�n estamos ya en condiciones de dise�ar m�dulos espec�ficos a medida.
*
*/ 

//-------------------------------------------------------
//Pagina3 Modulos Especificos
//-------------------------------------------------------
/**
 * \page ModulosEspecificos 3.2.- M�dulos Espec�ficos
 *
 */
