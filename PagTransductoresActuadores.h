/**
******************************************************
* @file PagModuloBasico.h
* @brief Documentacion del m�dulo B�sico
* @author FabLab Lleida
* @author Jose Martinez Molero
* @author Julian Salas Bartolome
* @version 1.0
* @date Abril 2018
*
*
*******************************************************/


//-------------------------------------------------------
//Este documento NO TIENE Codigo, simplemente contiene documentacion para Doxygen  
//-------------------------------------------------------



/**
 * \section TransductoresActuadores 3.- Trasnductores y Actuadores
 *
 * Los Transdcutores y Actuadores son lo elementos del sistema que van a conectar los dispositivos a telecontrolar a la centralita ServerPic
 *
 *
 *
 * Estos m�dulos, por un lado, se encarga�n de la comunicaci�n con ServerPic, por otro, dispondr�n de la electr�nica necesaria
 * para captar los parametros f�sicos del entorno y/o para conseguir transformar en actuaciones lar ordenes recibidas desde el sistema.
 *
 * Todos los m�dulos, por tanto, tendr�n una parte com�n que ser� la encaragda de la comunicaci�n y otra parte espec�fica que depender� del 
 * cometido del m�dulo y de su electr�nica.
 * 
 * La parte com�n se ha desarrollado entorno a un m�dulo , denominado <b>M�dulo B�sico</b>, que se encargar� de conectar el dsipositivo al  sistema y de 
 * la comunicaci�n con el.
 *
 * A partir del software del <b>m�dulo B�sico</b> se desarrollan los m�dulos para aplicaciones espec�ficas. Cada
 * m�dulo espec�fico tendr� una electr�nica concreta y una peuqe�a parte del software adecuada para controlar esa electr�nica. El software, se encargara de mandar informaciones a otros m�sdulos
 * y/o de recibirlas desde otros.
 *
 * Procncipalmente, para la comunicaci�n entre m�dulos se utilizar� el comando <b>mensaje</b> de ServerPic descrito en el apartado \ref ServerPic12 del cap�tulo ServerPic
 *
 * El m�dulo <b>emisor</b> env�a un mensaje con el formato
 *
 *		mensaje-:-<Cliente Receptor>-:-Texto Mensaje<intro>
 *
 * El m�dulo <b>receptor</b> recibe ese mensaje con el formato
 *
 *		<Cliente Remitente>-:-Texto Mensaje<intro>
 *
 * El m�dulo <b>receptor</b> analizar� el campo <b>Texto Mensaje</b> que contiene la orden que posteriormente ejecutar�. 	 
 *  
 * Hecha una peque�a introducci�n, el tema Tarnsductores y Actuadores se va a desarrollar seg�n el siguiente indice
 *
 * \section IndiceTrasductoresyActuadores Indice
 *    	- \ref Basico1
 *				- \ref Basico11
 *				- \ref Basico12
 *					- \ref Basico121
 *					- \ref Basico122
 *				- \ref Basico122
 *    	- \ref SeccionEspecificos
 *			- \ref SeccionOutlet21
 * 				- \ref SeccionOutlet211
 * 				- \ref SeccionOutlet212
 * 				- \ref SeccionOutlet213
 *
 *
 *
 */
//-------------------------------------------------------
//Pagina2 Modulos Especificos
//-------------------------------------------------------
/**
 * \page TransductoresyActuadores 3.- Transductores y Actuadores
 *
 */
