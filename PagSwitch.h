/**
******************************************************
* @file PagSwitch.h
* @brief Documentacion de ModuloS Switch
* @author FabLab Lleida
* @author Jose Martinez Molero
* @author Julian Salas Bartolome
* @version 1.0
* @date Junio 2018
*
*
*******************************************************/

//-------------------------------------------------------
//Este documento NO TIENE Codigo, simplemente contiene documentacion para Doxygen  
//-------------------------------------------------------

/**
 * \section SeccionSwitch322 3.2.2.- Switch (Pulsador)
 * \subsection Seccionswitch3221 3.2.2.1.- Descripci�n general
 *
 * El dispositivo Switch se ha dise�ado para realizar la funci�n de pulsador, interruptor y dispositivos derivados de estos.
 * 
 * Un pulsador solo no tiene sentido, cuando se actue sobre el se deber� enviar un mensaje a otro dispositivo alertandole de que 
 * se ha actuado sobre el pulsador, por eso, en ServerPic.h se declarara el cliente de destino del pulsador.  
 * 
 * Al igual que Outlet, Switch adem�s de las ordenes de servicio, recibir� las ordenes para poder conectar/desconectar el m�dulo.
 * 
 * La lista de ordenes que podr� recibir son las mismas que para Outlet
 *
 *		On.- Habilita el Switch
 *		Off.- DesHabilita el Switch
 *		Change.- Cambia de estado (Habilitado/deshabilitado) del Swtich
 *		Get.- Devuelve el estado de Switch al dispositivo que se lo solicita.
 *		ChangeGet.- Cambia el estado del Switch y duvelve el nuevo estado al remitente.

 *
 * \subsection Seccionswitch3222 3.2.2.2.- Hardware
 *
 * El hardware ser� el mismo que el utilizado en el Outlet pero reemplazando la parte de Rel�  por un simple pulsador
 *
 * \image html ..\..\Documentacion\Imagenes\Switch.JPG
 *
 *
 * \subsection Seccionswitch3223 3.2.2.3.- Software
 *
 * El software conservar� la estructura de M�dulo B�sico y respecto a Outlet habr� varios cambios.
 *
 * Los principales problemas para dise�ar el software para atender un pulsador son dos:
 *
 *	- No perder pulsaciones
 *	- Eliminar los rebotes
 *
 * El primero lo solucionaremos asociando una interrupci�n al pulsador. La fucni�n de servicio de esa interrupci�n
 * se encargar� de poner un flag a <b>1</b>. El programa principal ( loop() ), testeara ese Flag y si est� a <b>1</b>, 
 * mandar� un mensaje al dispositivo asociado al Switch y resetear� el flag poniendolo de nuevo a <b>0</b> a la espera de una nueva puslaci�n..
 *
 * Para eliminar los rebotes, a la hora de atender la interrupci�n del pulsador, se comprobar� que ha transcurrido un tiempo prudencial
 * desde la ultima atenci�n de interrupci�n para poner el Flag a 1, si no ha transcurrido ese tiempo, no se actualizar� el flag. De esta forma
 * despreciaremos todos los impulsos que se produzcan despu�s de una pulsaci�n ( rebotes )
 *
 * Para llevar todo eso a la pr�ctica, en IO.h  definiremos PinPulsador. 
 * 
 * Actualizamos IO.h
 *
 *\code
 *#ifndef IO_H
 *	#define IO_H
 *
 *	#ifdef ESP01
 *		#define Modelo "ESP01"
 *		int PinReset = 0;
 *		int PinPulsador = 2;													
 *	#endif	
 *		
 *	#ifdef ESP12
 *		#define Modelo "ESP12"	
 *		int PinReset = 0;
 *		int PinPulsador = 13;													
 *	#endif	
 *
 *
 *#endif
 *\endcode
 *
 * Tambi�n ser� necesario definir en ServerPic.h el Cliente de ServerPic que recibir� el aviso de pulsador <b>Pulsado</b> y por �ltimo, se deber� definir el 
 * tiempo en msg que consideramos necesario para poder absorber rebotes.
 *\code
 *	//----------------------------------------------
 *	//TiEmpo de rebotes
 *	//----------------------------------------------
 * 	#define TempoRebotes 150
 *
 *	//----------------------------------------------
 *	//Cliente de destino
 *	//----------------------------------------------
 *	#define ClienteDestino "Outlet1"
 *\endcode
 *
 * En la declaraci�n de variables, se incluir�n las nuevas variables que vamos a necesitar
 *
 *\code
 * 	ESP8266WebServer server(80);		//Definimos el objeto Servidor para utilizarlo en Modo AP
 *	WiFiClient Cliente;			//Definimos el objeto Cliente para utilizarlo en Servidor
 *	Telegrama oMensaje;			//Estructura mensaje donde se almacenaran los mensajes recibidos del servidor
 *
 *	long nMiliSegundosTest = 0;		//Variable utilizada para temporizar el Test de conexion del modulo a ServerPic
 *	volatile long nMilisegundosRebotes = 0;	//Variable utilizada para temporizar el tiempo de absorcion de rebotes
 *
 *	volatile boolean lFlagInterrupcion = 0;	//Flag para indicar que ha habido interrupci�n 
 *
 *	boolean lEstado = 0;			//Flag donde se almacena el estado Habilitado/Deshabilitado del Switch
 *	String cSalida;				//Variable con el texto que se enviara en los mensajes
 *
 *\endcode
 *
 * En setup() declararemos la interrupci�n asociada al pulsador
 *
 *\code
 *	void setup() {
 *		.
 *		.
 *		.
 *		if ( ClienteServerPic () )								//Intentamos conectar a ServerPic
 *		{
 * 				#ifdef Debug
 *					Serial.println(" ");
 *					Serial.println("Conectado al servidor");
 * 				#endif    
 * 				#ifdef HomeKit
 * 					DataConfig aCfg = EpromToConfiguracion ();  			//Leemos la configuracin de la EEprom
 * 					char USUARIO[1+aCfg.Usuario.length()]; 				//Almacenamos en el array USUARIO el nombre de usuario 
 *					(aCfg.Usuario).toCharArray(USUARIO, 1+1+aCfg.Usuario.length());          
 *					cDispositivo = USUARIO;
 *				#endif
 *				attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccion, FALLING); //Declaracion interrupcion
 *
 * 		}
 *		.
 *		.
 *		.
 *	}
 *\endcode
 * 
 * Ahora se debe incluir una rutina de servicio de la interrupci�n que lo que har� ser� poner el 
 * Flag de la interrupci�n a <b>1</b> si se han superado los rebotes.
 *
 *\code
 *	void ISRAccion (void)
 *	{
 * 		if (millis() > nMilisegundosRebotes + TempoRebotes)
 *		{
 *			lFlagInterrupcion = 1;
 *			nMilisegundosRebotes = millis();
 *		}
 *
 *	}
 *\endcode
 *
 * Pocos comentarios tiene esta funci�n, si se provoca una pulsaci�n, se entra en esta funci�n que pone el Flag de 
 * interrupci�n a <b>1</b> y registra el instante de la pulsaci�n ( en nMilisegundosRebotes ) para descartar
 * las siguientes interrupciones recibidas antes de rebasar el tiempo establecido para eliminar los rebotes.
 *
 * Ahora, una vez detectada la pulsaci�n y puesto el Flag de interrupcion a <b>1</b>, se debera comprobar el estado del flag en
 * loop() y actuar mandando un mensaje al cliente asignado.
 * 
 * Al mismo tiempo, se actualiza el estAdo del pulsador en caso de perdida de conexion, se deshabilita si se pierde la conexi�n y
 * cuando se repuera la conexi�n se restablece al estado anterior a la perdida.
 *
 *\code
 void loop() {
 
 	------------------
 	Comprobacion Reset
 	------------------

	TestBtnReset (PinReset);

	---------------------
	Comprobacion Conexion
	---------------------

	if ( TiempoTest > 0 )
	{
		if ( millis() > ( nMiliSegundosTest + TiempoTest ) )	//Comprobamos si existe conexion  
		{
			nMiliSegundosTest = millis();
			if ( !TestConexion() )				//Si se ha perdido la conexion
			{
				lConexionPerdida = 1;			//Ponemos el flag de perdida conexion a 1
				detachInterrupt(PinPulsador);	
			}else{						//Si existe conexion
				if ( lConexionPerdida )			//Comprobamos si es una reconexion ( por perdida anterior )
				{					//Si lo es
					lConexionPerdida = 0;		//Reseteamos flag de reconexion
					if ( lEstado )			//Y dejamos el Pulsador en el estado anterior a la perdida
					{
						attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccion, FALLING);							
					}	
				}	
			}												                                                         	
		}	
	}

	----------------------
	Comprobacion Pulsacion
	----------------------
 
	if ( lFlagInterrupcion == 1)				//Si se ha pulsadao el pulsador ( ha habido interrupcion )
	{
		delay (TempoRebotes);
		if ( !digitalRead(PinPulsador))				//Comprobamos que sigue pulsado el pulsador trancurrido un tiempo	
									//( garantizamos que solo hay respuesta en flanco de bajada, 
									//los rebotes del flanco de subida podrian dar falsas pulsaciones)
		{							//Si sigue pulsado
			lFlagInterrupcion=0;				//Reseteamos el falg de interrupcion 
			oMensaje.Mensaje = "Change";			//Mandamos change al destinatario
			oMensaje.Destinatario = ClienteDestino;
			EnviaMensaje(oMensaje);
		}		
	}
 
	------------------
	Analisis Comandos
	------------------

	oMensaje = Mensaje ();					//Iteractuamos con ServerPic,comprobamos si se ha recibido alg�n mensaje
	if ( oMensaje.lRxMensaje)					//Si se ha recibido ( oMensaje.lRsMensaje = 1)
	{
		.
		.
		-----------------------
		Actualizaci�n WebSocket
		-----------------------
		.
		.
		--------------------
		Notificacion HomeKit
		--------------------
		.
		.
	}
	
	wdt_reset(); 						//Refrescamos WDT	
}
 *\endcode
 *
 * En el bloque <b>loop</b>, en el apartado reservado a <b>Iteractua con ServerPic</b> se analizar� la orden recibida y se actuara seg�n ella. 
 * habilitando o deshabilitando la interrupci�n para habilitar/Deshabiitar el Switch
 *\code
 oMensaje = Mensaje ();				//Iteractuamos con ServerPic, comprobamos si sigue conectado al servidor y si se ha recibido algun mensaje

if ( oMensaje.lRxMensaje)			//Si se ha recibido ( oMensaje.lRsMensaje = 1)
{
   	#ifdef Debug				
		Serial.println(oMensaje.Remitente);//Ejecutamos acciones
		Serial.println(oMensaje.Mensaje);
	#endif	
	
	if (oMensaje.Mensaje == "On")		//Si se recibe "On", Habilitamos la interrupcion
	{	
		lEstado = 1;
		attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccion, FALLING);	
		cSalida = "On";
	}
	if (oMensaje.Mensaje == "Off")		//Si se recibe "Off", deshabilitamos la interrupcion
	{	
		lEstado = 0;
		detachInterrupt(PinPulsador);	
		cSalida = "Off";
	}
	if (oMensaje.Mensaje == "Change")	//Si se recibe 'Change', cambia el estado habilitado/deshabilitado
	{	
		if ( lEstado )
		{
			lEstado = 0;
			detachInterrupt(PinPulsador);
			cSalida = "Off";
		}else{
			lEstado = 1;
			attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccion, FALLING);	
			cSalida = "On";
		}
	}
	if (oMensaje.Mensaje == "ChangeGet")	//Si se recibe 'ChangeGet', cambia el estado habilitado/deshabilitado y devuelve el nuevo estado al remitente 
	{	
		if ( lEstado)
		{
			lEstado = 0;
			detachInterrupt(PinPulsador);
			cSalida = "Off";
		}else{
			lEstado = 1;
			attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccion, FALLING);	
			cSalida = "On";
		}
		oMensaje.Mensaje = cSalida;	//Confeccionamos el mensaje a enviar hacia el servidor	
		oMensaje.Destinatario = oMensaje.Remitente;
		EnviaMensaje(oMensaje);		//Y lo enviamos
	}			
	if (oMensaje.Mensaje == "Get")		//Si se recibe 'Get', se devuelve el estado de habilitado/deshabilitado
	{	
		if ( lEstado )
		{
			cSalida = "On";
		}else{
			cSalida = "Off";
		}
		oMensaje.Mensaje = cSalida;
		oMensaje.Destinatario = oMensaje.Remitente;
		EnviaMensaje(oMensaje);	
		cSalida = String(' ');		//No ha habido cambio de estado, Vaciamos cSalida para que no se envie a WebSocket y a HomeKit 
	}	
	if ( cSalida != String(' ') )		//Si hay cambio de estado
	{	
		EnviaMensajeWebSocket(cSalida);	//Actualizamos las p�ginas html conectadas
	}
	#ifdef HomeKit
		if ( cSalida != String(' ') )
		{
			oMensaje.Destinatario = cDispositivo + String("_");
			oMensaje.Mensaje = cSalida;
			EnviaMensaje(oMensaje);
		}
	#endif		
	cSalida = String(' ');

}
 *\endcode
 *
 * \section Seccionswitch3224 3.2.2.4.- Pulsador con leds se�alizacion 
 * 
 * Este dispositivo es una particularidad de Switch y se diferencia de el en que ahora se utilizan los led conectados a Tx y Rx
 * para indicar el estado.
 * 
 * El led rojo encendido indicara que el pulsador tiene conexion con el servidor y esta habilitado, el led verde indicara que 
 * el dispositivo que controla el pulsador esta encendido.
 * 
 * En primer lugar, definiremos en IO.h los pin de los Led
 * 
 *\code
 * #ifndef IO_H
 *	#define IO_H
 *
 *	#ifdef ESP01
 *		#define Modelo "ESP01"
 *		int PinReset = 0;														
 *		int PinSalida = 2;8 
 *		int PinLedRojo = 3;
 *		int PinLedVerde = 1;	
 *	#endif
 *	#ifdef ESP12
 *		#define Modelo "ESP112"
 *		int PinReset = 0;														
 *		int PinSalida = 2;														
 *		int PinLedRojo = 3;
 *		int PinLedVerde = 1;			
 *	#endif 
 *\endcode
 *
 * Luego, en PulsadorLeds.ino definiremos las funciones para encender/apagar los led
 *
 *\code
 * void EnciendeLedVerde ()
 * {
 *	digitalWrite(PinLedVerde, HIGH);
 * }
 * void ApagaLedVerde ()
 * {
 *	digitalWrite(PinLedVerde, LOW);
 * }
 * void EnciendeLedRojo ()
 * {
 *	digitalWrite(PinLedRojo, HIGH);
 * }
 * void ApagaLedRojo ()
 * {
 *	digitalWrite(PinLedRojo, LOW);
 * } 
 *\endcode 
 * 
 * Ahora en loop() utilizaremos los led para indicar el estado como creamos conveniente, por ejemplo, el led rojo puede indicar
 * si el pulsador esta habilitado o no. 
 * El Led verde puede utilizarse para indicar el estado del dispositivo controlado por el pulsador, para ello, el dispositivo controlado
 * deber� enviar un mensaje al pulsador indicando el estado. Esto es facil si el pulsador manda un 'ChangeGet' al dispositivo por que este le 
 * le responder� al pulsador con un 'On' u 'Off' que se podria emplear para encender o apagar el led verde el problema es que si 
 * el estado del dispositivo se cambia desde otro cliente del sistema, el led verde del pulsdaor no se actualizar�.
 * 
 * El c�digo de loop() podr�a ser:
 * 
 * \code
		oMensaje = Mensaje ();					//Iteractuamos con ServerPic, comprobamos si sigue conectado al servidor y si se ha recibido algun mensaje

		if ( oMensaje.lRxMensaje)				//Si se ha recibido ( oMensaje.lRsMensaje = 1)
		{
			#ifdef Debug				
				Serial.println(oMensaje.Remitente);	//Ejecutamos acciones
				Serial.println(oMensaje.Mensaje);
			#endif	
	
			if (oMensaje.Mensaje == "On")			//Si se recibe "On", Encendemos el led verde
			{	
				EnciendeLedVerde();
			}
			if (oMensaje.Mensaje == "Off")			//Si se recibe "Off", Apagamos el led verde
			{	
				ApagaLedVerde();
			}
			if (oMensaje.Mensaje == "Change")		//Si se recibe 'Change', cambia el estado habilitado/deshabilitado
			{	
				if ( lEstado )
				{
					lEstado = 0;
					detachInterrupt(PinPulsador);
					cSalida = "Off";
					ApagaLedRojo();
				}else{
					lEstado = 1;
					attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccion, FALLING);	
					cSalida = "On";
					EnciendeLedRojo();
				}
			}
			if (oMensaje.Mensaje == "ChangeGet")		//Si se recibe 'ChangeGet', cambia el estado habilitado/deshabilitado e informa al remitente 
			{	
				if ( lEstado)
				{
					lEstado = 0;
					detachInterrupt(PinPulsador);
					cSalida = "Off";
					ApagaLedRojo();
				}else{
					lEstado = 1;
					attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccion, FALLING);	
					cSalida = "On";
					EnciendeLedRojo();
				}
				oMensaje.Mensaje = cSalida;		//Confeccionamos el mensaje a enviar hacia el servidor	
				oMensaje.Destinatario = oMensaje.Remitente;
				EnviaMensaje(oMensaje);			//Y lo enviamos
			}			
			if (oMensaje.Mensaje == "Get")			//Si se recibe 'Get', se devuelve el estado de habilitado/deshabilitado
			{	
				if ( lEstado )
				{
					cSalida = "On";
				}else{
					cSalida = "Off";
				}
				oMensaje.Mensaje = cSalida;
				oMensaje.Destinatario = oMensaje.Remitente;
				EnviaMensaje(oMensaje);	
				cSalida = String(' ');			//No ha habido cambio de estado, Vaciamos cSalida para que no se envie a WebSocket y a HomeKit 
			}	
			if ( cSalida != String(' ') )			//Si hay cambio de estado
			{	
				EnviaMensajeWebSocket(cSalida);		//Actualizamos las p�ginas html conectadas
			}
			#ifdef HomeKit
				if ( cSalida != String(' ') )
				{
					oMensaje.Destinatario = cDispositivo + String("_");
					oMensaje.Mensaje = cSalida;
					EnviaMensaje(oMensaje);
				}
			#endif		
			cSalida = String(' ');

		}

 * \endcode
 *  
 * \section Seccionswitch3225 3.2.2.5.- PIR
 *
 * En este apartado se va a describir como montar un PIR partiendo del dsipostivivo Switch
 *
 * Para conseguir un PIR, sustituiremos el pulsador por un dispositivo sensor de movimiento, cualquier sensor puede servir aunque 
 * hay que tener en cuenta las alimentacion. Un sensor muy utilizado en el 'mundo arduino' es el HC-SR501. Este dsipsitivo es muy sencillo
 * y muy econ�mico, existe gran cantidad de informaci�n sobre el en internet por lo que no es necesario profundizar. 
 *
 * El Software es muy similar al de Pulsador.ino.
 *
 * Primero cambiamos el sentido del flanco qu eprovoca la interrupci�n, al contrario que el pulsador, el pin data  HC-SR501 est� normalmente
 * a <b>0</b> y pasa a <b>1</b> durante un tiempo regulable en el momento que se detecta movimiento, por tanto, se provocar� la interrupci�nn
 * en el flanco ascendente. En cada l�nea donde se activa la interrupci�n se deber� cambiar <b>FALLING</b> por <b>RISING</b>
 *
 * Por otro lado, cuando  lFlagInterrupci�n est� a <b>1</b> ( se ha detectado movimiento ) se envia al ClienteDestino el texto <b>Alarma</b>, el 
 * ClienteDestino interpretar� ese texto y actuar� en consecuencia ( har� sonar una sirena, encender� una luza....), ese mismo texto lo enviar� 
 * a WebSocket que permitira a las paginas html cambiar la imagen del PIR habilitado por la de PIR en alarma.
 *
 * Tambi�n hemos aprovechado este dispositivo para mandar una alerta a un m�vil meidante PushOver aunque, de momento, solo en plan experimental.
 *
 *\code
     	if ( lFlagInterrupcion == 1)											//Si se ha detectado movimiento ( ha habido interrupcion )
		{
			delay (TempoRebotes);
			if ( digitalRead(PinPulsador))										//Comprobamos que sigue pulsado el pulsador trancurrido un tiempo	( garantizamos que solo hay respuesta en flanco de bjado, los rebotes del flanco de subida podrian dar falsas pulsaciones)
			{																	//Si sigue pulsado
				wdt_reset();
				lFlagInterrupcion=0;											//Reseteamos el falg de interrupcion 
				oMensaje.Mensaje = "Alarma";     									//Mandamos change al destinatario
				oMensaje.Destinatario = ClienteDestino;
				EnviaMensaje(oMensaje);
				cSalida = "Alarma";
				wdt_reset();
				EnviaMensajeWebSocket(cSalida);			//Actualizamos las p�ginas html conectadas
				#ifdef HomeKit
					oMensaje.Destinatario = cDispositivo + String("_");
					oMensaje.Mensaje = cSalida;
					wdt_reset();
					EnviaMensaje(oMensaje);
				#endif	
				Cliente.print ("push-:-Iphone_Julian-:-Alarma-:-Deteccion de PIR-:-cosmic");	
				Cliente.print("\n");				
				cSalida = String(' ');
			}		
		}
 *
 *\endcode
 *
 * \section Seccionswitch3226 3.2.2.6.- Alarma de Puerta/Ventana
 *
 * En este apartado se va a describir como montar una alarma de apertura para puerta o ventana partiendo del dsipostivivo Switch
 *
 * Para conseguir una alarma para puerta o ventana partiremos de los dispostivos existentes en el mercado que se pueden encontrar 
 * en cualquier ferreteria o 'tienda de los chinos'.
 *
 * Esos dispositivos estan basados en la apertura/cierre del contacto de una ampolla reed por el efecto de un iman que, cuando la puerta/ventana
 * estan cerradas, el iman est� pegado a la ampolla y elcontacto de la misma se encuentra abierto y en caso de apertura, el iman se separa de
 * la ampolla y se cierra el contacto. Practicamente similar al pulsador o al PIR pero en este caso vamos a introducir una novedad en el 
 * funcionamiento de los ESP8266.
 *
 * Se debe pensar que muy posiblemente estos dispositivos no puedan ser alimentados mediante la red el�ctrica, deberemos recurrir a pilas, pero,
 * cuanto le durar�a una pila a un ESP8266 con su wifi activado?, la respuesta seguro que nos har�a descartar utilizar este tipo de dispositivos, y es aqui
 * donde aparece la novedad a la que se hacia referencia en el parrafo anterior, vamos a poner a 'dormir' a nuestro ESP que solo se despertara cuando
 * detecte una apertura, una vez despierto, se conecta�a a la SSID y al servidor, mandar� los mensajes que se establezcan para avisar de la situacion de alarma
 * y volver� a la posici�n de 'dormir'.
 *
 * Esta t�cnica la tiene prevista el ESP para <b>ahorro de energ�a</b> ya que mientras duerme el consumo es de 20 uA y el tiempo que esta despierto para transmitir la alarma
 * es de unos pocos segundos por lo que ahora ya se hace viable alimentarlo con pilas.
 *
 * El Hardware es similar al presentado para el pulsador pero sustituyendo el pulsador por una ampolla reed y, en ved de conectarlo a GPIO2 ( IO1 ), 
 * lo conectaremos al Reset del ESP8266.
 *
 * El funcionamiento es el siguiente, cuando el dispositivo arranca se poen el flag <b>lEstado</b> a <b>1</b> en el bloque setup(), lueogo, en 
 * el bloque loop() se testea el flag y si est� a <b>1</b>, envia mensaje a ClienteDestino, WebSocket y HomeKit y acto seguido se ejecuta la instrucci�n
 *\code
 		ESP.deepSleep(TIEMPO_DeepSleep, WAKE_RF_DEFAULT);	
 *\endcode
 *
 * Esa instrucci�n lo que hace es poner a dormir al ESP y arranca un TImer sobre GPIO16 de forma que ese pin se pone a 0 durante un
 * tiempo definido en TIEMPO_DeepSleep, transcurrido ese tiempo el pin GPIO16 se pone a 1 de forma que, si se une GPIO16 con el pin de 
 * reset, cuando se llegue a esa insturccion, el ESP se pone a dormir y se despierta al poner un 1 en reset al cabo de  TIEMPO_DeepSleep us.
 *
 * Existen bastantes sitios donde se explica el modo deepSleep y todas sus posibilidades pero para nuestro caso, no haremos la union GPIO16 y Reset
 * por que no nos interesa despertarlo al cabo de un tiempo, lo que queremos es despertarlo cuando se abra la puerta o ventana, es decir, 
 * cuando se separe el iman del dispositivo que es cuando la ampolla reed abrir� su contacto y pondra un <b>1</b> en reset.
 *
 * Es lo mismo que el montaje estandar de deepSleep salvo que en lugar de que GPIO16 ponga un <b>1</B> en reset, lo hace la ampolla con el iman.
 *
 * El bloque setup() quedar� as�
 *
*\code
void setup() {
	.
	.

	if ( LeeByteEprom ( FlagConfiguracion ) == 0 )			//Comprobamos si el Flag de configuracion esta a 0
	{								// y si esta
		ModoAP();						//Lo ponemos en modo AP
	}else{								//Si no esta
		if ( ClienteSTA() )					//Lo poenmos en modo STA y nos conectamos a la SSID
		{							//Si ha conseguido conectarse a ls SSID en modo STA
			if ( ClienteServerPic () )							//Intentamos conectar a ServerPic
			{
				.
				.
				lEstado = 1;				//Ponemos el flag a 1 por que se ha arrancado por apertura
			}
		}	
	}
}
*\endcode
*
* No hay nada de extraordinario en este bloque, cuando arranca el ESP suponemos que es por que se abrio la puerta o ventana
* y si logra conectarse al servidor, se pone el flag  lEstado a <b>1</b>
*
* En el bloque loop() el codigo quedar�a as�
*
*\code
void loop() {

		----------------
 		Comprobacion Reset
 		------------------

		TestBtnReset (PinReset);

 		---------------------
 		Comprobacion Conexion
 		----------------------
		.
		.

		----------------
 		Analisis comandos
 		------------------

		oMensaje = Mensaje ();					//Comprobamos si se ha recibido algun mensaje

		if ( oMensaje.lRxMensaje)				//Si se ha recibido ( oMensaje.lRsMensaje = 1)
		{

			.
			.

		}
		cSalida = String(' ');
		if ( lEstado )
		{	
			wdt_reset();

			oMensaje.Mensaje = "Alarma";			//Mandamos Alarma al destinatario
			oMensaje.Destinatario = ClienteDestino;
			EnviaMensaje(oMensaje);

	 		--------------------
 			Notificacion HomeKit
 			---------------------

			#ifdef HomeKit
				oMensaje.Destinatario = cDispositivo + String("_");
				oMensaje.Mensaje = cSalida;
				EnviaMensaje(oMensaje);
			#endif		

			wdt_reset();	

	 		-----------------------
 			Actualizacion WebSocket
 			------------------------
			cSalida = "On";			
			EnviaMensajeWebSocket(cSalida);			//Actualizamos las p�ginas html conectadas

			wdt_reset();					

	 		-----------------------
 			Actualizacion PushOver
 			------------------------

			cSalida = String(' ');
			Cliente.print ("push-:-Iphone_Julian-:-Alarma-:-Apertura de puerta-:-cosmic");	
			Cliente.print("\n");				

			cSalida = String(' ');

			Cliente.stop();
			ESP.deepSleep(TIEMPO_DeepSleep, WAKE_RF_DEFAULT); //Se pone ESP8266 a dormir

		}			
	    wdt_reset(); 						//Refrescamos WDT

}
*\endcode
*
* En esta versi�n no se ha previsto que el dispositivo reciba comandos, por tanto el bloque donde se analizan os comandos estar� vacio.
*
* Fuera del bloque de analisis de comnados, lo que se hace es testear el Flag lEstado, si es <b>1</b>, en primer lugar, se envia mensaje de Alarma
* al Cliente de destino, luego se manda el texto Alarma a WebSocket para reflejar la intrusi�n en las paginas html que puedan estar abiertas y posteriormente
* , si procede, se notificar� a HomeKit.
*
* Una vez notificada la alarma a los clientes que proceda, se cierra la conexi�n con el servidor y se 'pone a dormir' al ESP8266 
*
*/

//-------------------------------------------------------
//Pagina2 Modulos Especificos
//-------------------------------------------------------
/**
 * \page Switch 3.2.2.- M�dulo Switch
 *
 */