
/**
******************************************************
* @file PagIndice.h
* @brief Indice de la documentacion
* @author FabLab Lleida
* @author Jose Martinez Molero
* @author Julian Salas Bartolome
* @version 1.0
* @date Abril 2018
*
*
*******************************************************/

/**
 * \mainpage Dispositivos dom�ticos
 *
 * 
 * Un sistema dom�tico se encarga de gestionar los servicios de una vivienda desde una central que sea capaz  de facilitar el telecontrol de todos ellos. 
 *
 * \image html Documentacion\Imagenes\InstalacionDomotica.png
 * \image html Documentacion\Imagenes\InstalacionDomotica.jpg
 *
 * El sistema est� formado por: 
 *		- Sensores.- Dsipositivos que se encargan de leer las magnitudes f�sicas del entorno y trasladarlas al sistema.
 *		- Actuadores.- Dispositivos que se encargan de convertir ordenes recibidas desde el sistema a procecesos fisicos.
 *		- Interfaces. Puntos desde donde se puede establecer comunicaci�n entre el sistema y los usuarios.
 *		- Centralita.- Dsipositivo encargado de gestionar la informaci�n entre sensores, actuadores e interfaces. 
 * 
 * En esta documentaci�n se describir� la composici�n y funcionamiento de un sistema dom�tico.
 *
 * Aunque existen varias soluciones para la comunicacion entre los componentes ( cada una con sus pros y sus contras ), se ha optado por la comunicacion
 * WIFI. Montar una infraestructura WIFI es sencillo y econ�mico y, aunque tiene ciertos inconvenientes, es la opci�n elegida.
 * 
 * Como centralita se ha optado por utilizar un servidor propio ( ServerPic ) corriendo en una Raspberry.
 *
 * Para sensores y actuadores, aunque hay muchas posibilidades se ha optado por desarrollarlos en torno al modulo ESP8266
 * 
 * Aunque se han dise�ado varios sensores y actuadores no es un sistema cerrado, la descripci�n del software permitir� dise�ar
 * nuevos elementos en funci�n de las necesidades y la elecrr�nica disponible. Se trata pues de proporcionar herramientas y conocimientos
 * para poder desarrollar unos m�dulos lo m�s simples y econ�micos posibles que puedan iteractuar entre ellos y puedan ser telecontrolados desde internet.
 *
 * Aunque el proyecto se pretende impulsar desde el FabLab de Lleida, es una apuesta personal que viene ya de hace unos cuantos a�os segun se describe en el 
 * apartado \ref Historia1
 *
 * La documentaci�n se estructura seg�n el siguiente indice
 *
 * \section Indice Indice
 *
 *    - \ref Historia1
 *    - \ref ServerPic1
 *    - \ref TransductoresActuadores
 *    	- \ref Basico1
 *    	- \ref SeccionEspecificos
 *			- \ref SeccionOutlet21
 *			- \ref SeccionSwitch322
 *				- \ref Seccionswitch3224
 *				- \ref Seccionswitch3225			
 *				- \ref Seccionswitch3226			
 *				
 */

