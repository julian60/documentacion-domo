/**
******************************************************
* @file PreparacionESP8266n.h
* @brief Documentacion para preparar ESP8266 para ser programado mediante IDE Arduino
* @author Julian Salas Bartolome
* @version 1.0
* @date Abril 2018
*
*
*******************************************************/



//-------------------------------------------------------
//Este documento NO TIENE Codigo, simplemente contiene documentacion para Doxygen  
//-------------------------------------------------------

/**
 *
 * En este documento se describe el procedimeinto para preparar un modulo ESP8266 para ser programado mediante IDE Arduino
 *

 * \section SecPreparacionESP82661 Preparaci�n del ESP8266 
 * 
 * \subsection Hardware	1.- Hardware 
 * Se han probado con exito en los modelos EP-01 y EP-12E y Node-MCU.
 * 
 * \image html ..\..\Documentacion\Imagenes\ep-01_pins.png
 * \image html ..\..\Documentacion\Imagenes\ep-12e_pins.png
 * \image html ..\..\Documentacion\Imagenes\nodemcu_pins.png 
 *
 * \subsection Software	2.- Software
 *
 * \subsection Pyton 2.1.- Instalacion de Pyton
 *
 * Descargamos el ejecutable para Python 2.7.13 desde este enlace <a href="file:..\..\Doxygen\html\Documentacion\Software\python-2.7.13.msi">local</a>  o bien la ultima version 2.7.X desde el  <a href="https://www.python.org/downloads/">sitio oficial</a>
 *
 * Instalar la version de Python descargada
 *
 * A�adir el path donde se instalo Pyton en la variable path de las variables de entorno, para ello, en W10 ir a <b>Panel de control\Sistema y seguridad\Sistema</b> y seleccionar <b>Configuracion Avanzada del Sistema</b>, se abre
 * una ventana donde pulsaremos el boton <b>Variables de Entorno</b>, buscamos la variable <b>Path</b> en <b>Variables del sistema</b>, pulsamos <b>Editar</b> y a�adimos un nuevo path, en nuestro caso <b>C:\\Python27</b>
 *
 * \subsection esptool 2.2.- Instalacion de esptool
 *
 * Una vez instalado Python, abrimos una ventana de comandos y vamos al directorio donde se encuentra el ejecutable <b>pip.exe</b>, en nuestor caso <b>C:\\Python27\\Scripts</b>, en esa ventana ejecutamos la instruccion <b>pip install esptool</b> 
 * Automaticamente se instalara la aplicacion.
 *
 * A�adimos a <b>Path</b> de <b>Variables de Entorno</b> el path de <b>esptool</b>, en este caso <b>C:\\Python27\\Scipts</b>.
 *
 * \subsection Firware  2.3.- Carga del Firware en el modulo ESP8266
 *
 * \subsection DescargaFirware 2.3.1.-  Descarga del firware
 *
 * Aunque posiblemente se pueden encontrar un firware mas actualizado,  el que se ofrece en esta descarga <a href="file:..\..\Doxygen\html\Documentacion\Firware\v0.9.5.2 AT Firmware.bin">v0.9.5.2 AT Firmware.bin</a>  se ha probado de forma satisfactoria.  
 *
 * \subsection CargaFirware  2.3.2.- Carga del firware 
 *
 * Una vez descargado el ficehro bin con el firware se debe cargar en el ESP8266. Independientemente del modelo ESP8266, para poder grabar en el se deben hacer las conexiones descritas en la siguiente imagen:
 *
 * @warning <H3>El convertidor USB<->Serie debe operar con 3,3V</H3>
 *
 * \image html ..\..\Documentacion\Imagenes\Conexion.png
 *
 * Una vez realizadas las conexiones descritas, se conecta el Convertidor USB<->Serie, vamos a la carpeta donde se encuentra el Friware descargado y escribimos el siguiente comando:
 * 
 * Para el modelo EP-01:
 *
 * <b>esptool.py -p <USB-port-with-ESP8266> -b 115200 write_flash -ff 40m -fm qio -fs 4m 0x00000 "v0.9.5.2 AT Firmware.bin"</b>
 *
 * Para el modelo EP-12
 *
 * <b>esptool.py -p <USB-port-with-ESP8266> -b 115200 write_flash -ff 40m -fm dio -fs 32m 0x00000 "v0.9.5.2 AT Firmware.bin"</b>
 *
 * Siendo <USB-port-with-ESP8266> el puerto que gestiona el convertidor USB<->Serie.
 * 
 * Si hubiera errores en la grabaci�n se deberia probar con velocidades inferiores.
 *
 *
 */

//-------------------------------------------------------
//Pagina1 M�dulo B�sico 
//-------------------------------------------------------
/**
 * \page PreaparacionEsp8266 Preparaci�n del ESP8266
 *
 */








