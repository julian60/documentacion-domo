/**
******************************************************
* @file PagOutlet.h
* @brief Documentacion de ModuloS Outlet
* @author FabLab Lleida
* @author Jose Martinez Molero
* @author Julian Salas Bartolome
* @version 1.0
* @date Abril 2018
*
*
*******************************************************/



//-------------------------------------------------------
//Este documento NO TIENE Codigo, simplemente contiene documentacion para Doxygen  
//-------------------------------------------------------

/**
 * \section SeccionOutlet21 3.2.1.- Outlet
 * \subsection SeccionOutlet211 3.2.1.1.- Descripción general
 *
 * El dispositivo Outlet básicamente se ha ideado para conectar un aparato a la red eléctrica aunque, en funcion de la electrónica utilizada, puede utilizarse para otros cometidos.
 *
 * Outlet, independientemente de las ordenes de servicio, tendrá las oredenes  'particulares'  para 'Encender' o 'Apagar' el aparato bajo control. Además de esas ordenes,  dispone de otras añadidas.
 *
 * Los comandos que puede recibir Outlet son los siguientes:
 *
 *		On.- Enciende el aparato controlado
 *		Off.- Apaga el aparato controlado
 *		Change.- Cambia de estado el aparato controlado
 *		Get.- Devuelve el estado de Outlet al dispositivo que se lo solicita.
 *		ChangeGet.- Cambia el estado del aparato controlado y duvelve el nuevo estado al remitente.
 * 
 * Para acceder a estos comandos desde otro cliente de ServerPic, tal como se describía en  \ref TransductoresActuadores,  se tendrá que enviar al servidor los ordenes mediante el siguiente formato
 *
 *		mensaje-:-<Nombre Outlet>-:-<Comando>
 *
 * Suponiendo que el  outlet se haya conectado al servidor con el nombre Outlet1, para encender el dispositivo controlado, se debera mandar al servidor el siguiente texto
 *
 *		mensaje-:-Outlet1-:-On
 *
 * De igual modo se aplicarán el resto de comandos
 *
 *		mensaje-:-Outlet1-:-Off
 *		mensaje-:-Outlet1-:-Change
 *		mensaje-:-Outlet1-:-Get
 *		mensaje-:-Outlet1-:-ChangeGet
 *
 * Además de cambiar el estado mediante comandos, también se puede hacer mediante el pulsador que se ha contemplado para ello.
 *
 * Los principales problemas para diseñar el software para atender un pulsador son dos:
 *
 *	- No perder pulsaciones
 *	- Eliminar los rebotes
 *
 * El primero lo solucionaremos asociando una interrupción al pulsador. La fucnión de servicio de esa interrupción
 * se encargará de poner un flag a <b>1</b>. El programa principal ( loop() ), testeara ese Flag y si está a <b>1</b>, 
 * mandará cambiará el estado del diuspositivo, reseteará el flag poniendolo de nuevo a <b>0</b> a la espera de una nueva puslación y actualizara HomeKit y WebSocket si han sido habilitados.
 *
 * Para eliminar los rebotes, a la hora de atender la interrupción del pulsador, se comprobará que ha transcurrido un tiempo prudencial
 * desde la ultima atención de interrupción para poner el Flag a 1, si no ha transcurrido ese tiempo, no se actualizará el flag. De esta forma
 * despreciaremos todos los impulsos que se produzcan después de una pulsación ( rebotes )
 *
 * \subsection SeccionOutlet212 3.2.1.2.- Hardware
 * Como se dijo anteriormente, Outlet se ha pensado para conectar a la red cualquier aparato eléctrico, equivaldria a un enchufe telecontrolado. 
 * En el siguiente gráfico se observa un posible esquema de Outlet para telecontrolar un relé convencional, relé que podría sustituirse por uno de estado solido si lo que se desea telecontrolar
 * es un dispositivo conectado a la red eléctrica, en cualquier caso, es un esquema orientativo que puede ser adaptado a distintas necesidades.
 * El esquema consta de un bloque de alimentación  (POWER) donde se incluye,  fuente de alimentación conmutada de 5V y regulador de tensión de 3,3V, un bloque de LED's donde se han dispuesto dos led's  para
 * mostrar el estado del modulo utillizando los pines de Tx y Rx del ESP8266, el bloque que contiene el ESP8266 en el que se ha partido de la versión más básica, el ESP-01, un bloque con el relé que se encargará de conmutar la carga  
 * y un bloque (INPUT/OUTPUT) con los conectores para acceso desde el esterior a las E/S, boton de reset y Jumpers para programar el modulo con un convertidor USB/Serie 
 *
 * \image html ..\..\Documentacion\Imagenes\Outlet.JPG
 *
 * \subsection SeccionOutlet213 3.2.1.3.- Software
 *
 * Como se comnentó, para desarrollar dispositivos específicos se parte del software del módulo básico. Suponiendo que se incluyeron los ficheros en las librerias de arduino, para un dispositivo específico será necesario disponer de, 
 * el fichero *.ino correspondiente, el fichero ServerPic.h y el fichero IO.h
 *
 * El fichero <b>ServerPic.h</b> será el mismo que el descrito en el Módulo Básico. Como que que los modulos <b>Outlet</b> estarán permanentemente conectados al servidor, se define el tiempo de test de conexión con un valor superior a 0.
 *
 * El fichero <b>IO.h</b>, ademas de la asignación del pin reset incluido en el módulo básico, se le debe añadir el pin salida, pin donde se conectará el relé, el pin pulsador donde se conectará el pulasador para controlar el dispositivo localmente
 * el el pin led donde se conectará un led para monstrar el estado del dispositivo. El fichero quedará así 
 *
 *\code
 #ifndef IO_H
	#define IO_H

	#ifdef ESP01
		#define Modelo "ESP01"
		int PinReset = 0;														
		int PinSalida = 2;
		int PinPulsador = 1;	
		int PinLed = 3;												
	#endif
	#ifdef ESP12
		#define Modelo "ESP12"
		int PinReset = 0;														
		int PinSalida = 12;		
		int PinPulsador = 13;
		int PinPulsador = 14;													
	#endif	
#endif
 *\endcode
 *
 * El fichero 'ino' lo nombraremos como el dispositivo, en este caso será <b>Outlet.ino</b>. La estructura de este fichero será igual que la del módulo básico con las siguientes diferencias:
 * 
 * Hay que tener en cuenta que se utilizan los pin de Tx y Rx como pulsador y led, por tanto, cuando se utilice Debug, tanto el pulsador como el led deben quedar inhabilitados, por ello
 * cada vez que se deban utilizar se comprobara si esta definido Debug, si no lo está, esos pin no se podran utilizar.
 * \warning Esta comprobación solo afecta al software, no al hardware. Se debe tener en cuenta a la hora de hacer pruebas con el hardware.
 *
 * Los bloques de codigo del fichero ino quedarán de la siguiente forma
 *
 *
 * En el bloque <b>setup</b> se debe declarar <b>PinSalida</b> y <b>PinLed</b> como salidas, <b>PinPulsador</b> como entrada:
 *
 *
 *\code
	void setup() {

  		#ifdef Debug 					//Usamos el puereto serie solo para debugar	
			Serial.begin(9600); 			//Si no debugamos quedan libres los pines Tx, Rx para set urilizados
		#endif
		
		EEPROM.begin(128);				//Reservamos zona de EEPROM
		//BorraDatosEprom ( 0, 128 );			//Borramos 64 bytes empezando en la posicion 0, esta linea esta comentada por que normalemnte no se usa		

  		pinMode(PinReset, INPUT_PULLUP);           	//Configuramos el pin de reset como entrada
  		pinMode(PinSalida, OUTPUT);                	//Configuramos el pin del rele com salida
  	
  		#ifndef Debug 					//Habilitamoslos pines de Tx y Rx como pulsador y led si no hay Debug
  			pinMode(PinPulsador, INPUT_PULLUP);    	//Configuramos el pin del pulsador como entrada con resistencias de pull-up
  			pinMode(PinLed, OUTPUT);               	//configuramos el pin del led verde como salida
  		#endif	
  	
  		#ifndef  Debug					//Si no esta definido Debug habilitaremos la interrupcion del pulsador cuando no esta conectado al servidor
  			attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccionNC, FALLING); //Habilitamos el pulsador para operar aunque no se conecte al servidor 
		#endif
	
		if ( LeeByteEprom ( FlagConfiguracion ) == 0 )	//Comprobamos si el Flag de configuracion esta a 0
		{						// y si esta
			ModoAP();				//Lo ponemos en modo AP

		}else{						//Si no esta
			if ( ClienteSTA() )			//Lo poenmos en modo STA y nos conectamos a la SSID
			{					//Si jha conseguido conectarse a ls SSID en modo STA
				if ( ClienteServerPic () )	//Intentamos conectar a ServerPic
				{
					#ifdef Debug
						Serial.println(" ");
						Serial.println("Conectado al servidor");
					#endif    
					#ifdef HomeKit
						DataConfig aCfg = EpromToConfiguracion ();     				//Leemos la configuración de la EEprom
						char USUARIO[1+aCfg.Usuario.length()]; 
						(aCfg.Usuario).toCharArray(USUARIO, 1+1+aCfg.Usuario.length());		//Almacenamos en el array USUARIO el nombre de usuario 
						cDispositivo = USUARIO;
					#endif
				}
			}	
		}
		#ifndef  Debug					//Si no esta definido Debug habilitaremos la interrupcion del pulsador cuando  esta conectado al servidor
			attachInterrupt(digitalPinToInterrupt(PinPulsador), ISRAccionSC, FALLING);	//Habilitamos pulsador con dsipositivo conectado a ervidor
		#endif    
	}
 *\endcode
 *
 * En el bloque <b>loop</b>, en la <b>Comprobación de conexión</b>, si no está habilitado el Debug, se enciende el led mientras se hace la comprobación.
 *
 * En el apartado <b>Comprobación pulsación</b> lo que se hace es comprobar si se ha pulsado el pulsador y si es así,  cambia de estado el dispositivo y actualiza WebSocket y HomeKit si están habilitados
 *
 * En el apartado reservado a <b>Análisis Conmandos</b> primero si el Debug está deshabilitado se hace destellar el led, luego se analiza la orden recibida y se actua según ella. 
 *
 *\code
 	void loop() {

 		------------------
 		Comprobacion Reset
 		------------------
 		.
 		---------------------
 		Comprobacion Conexion
 		---------------------

		if ( TiempoTest > 0 )
		{
			if ( millis() > ( nMiliSegundosTest + TiempoTest ) )			//Comprobamos si existe conexion  
			{
			  	#ifndef  Debug							//Si no esta definido Debug
			        EnciendeLed();        				//Encendemos el led para indicar que se comprueba la conexion
			    #endif    
				nMiliSegundosTest = millis();
				if ( !TestConexion() )						//Si se ha perdido la conexion
				{
					lConexionPerdida = 1;					//Ponemos el flag de perdida conexion a 1
					if ( digitalRead(PinSalida) == 1 )			//Si el dispositivo estaba a On
					{
						lEstado = 1;					//Guardamos ese estado para tenerlo en cuenta en la reconexion
						digitalWrite(PinSalida, LOW);			//Ponemos a Off el dispositivo	
					}	
				}else{								//Si existe conexion
				  	#ifndef  Debug						//Si no esta definido Debug
				        ApagaLed();				//Apagamos el led para indicar que se ha finalizado el test                                                                                      
				    #endif    
					if ( lConexionPerdida )					//Comprobamos si es una reconexion ( por perdida anterior )
					{							//Si lo es
						lConexionPerdida = 0;				//Reseteamos flag de reconexion
						digitalWrite(PinSalida, lEstado);		//Ponemos el dispositivo en el estado anterior a la perdida de conexion
					}	
				}	
 	   		}	
		}

 		----------------------
 		Comprobacion Pulsacion
 		----------------------
		if ( lFlagInterrupcion == 1)								//Si se ha puladao el pulsador ( ha habido interrupcion )
		{
			delay (TempoRebotes);
			Inicio = millis();
			while ( !digitalRead(PinPulsador))
			{
  				wdt_reset();
			}  
			lFlagInterrupcion=0;                      					//Reseteamos el falg de interrupcion 
			if (digitalRead(PinSalida) == 1)                				//Si la salida estaba a uno ..... 
			{
				#ifdef Debug
					Serial.println("Dispositivo On");
				#endif
				digitalWrite(PinSalida, LOW);             					//La ponemos a 0
				cSalida = "Off";
			}else{                              						//Si no estaba a 1.....
				#ifdef Debug
					Serial.println("Dispositivo Off");
				#endif  
				digitalWrite(PinSalida, HIGH);              					//La ponemos a 1
				cSalida = "On";
			}

			----------------
 			Actualizacion WebSocket
 			------------------

			if ( cSalida != String(' ') )						//Si hay cambio de estado
			{	
				EnviaMensajeWebSocket(cSalida);					//Actualizamos las páginas html conectadas
			}
	 		
	 		----------------
 			Notificacion HomeKit
 			------------------

			#ifdef HomeKit
				if ( cSalida != String(' ') && oMensaje.Remitente != ( cDispositivo + String("_") ) )
				{
					oMensaje.Destinatario = cDispositivo + String("_");
					oMensaje.Mensaje = cSalida;
					EnviaMensaje(oMensaje);
				}
			#endif		
			cSalida = String(' ');
		} 

 		------------------
 		Analisis Comandos
 		------------------
		oMensaje = Mensaje ();								//Iteractuamos con ServerPic,comprobamos si se ha recibido algún mensaje
		if ( oMensaje.lRxMensaje)							//Si se ha recibido ( oMensaje.lRsMensaje = 1)
		{
			if (oMensaje.Mensaje == "On")						//Si se recibe 'On', se pone PinSalida a '1'
			{	
 				digitalWrite(PinSalida, HIGH);	
 			}
 			if (oMensaje.Mensaje == "Off")						//Si se recibe 'Off', se pone PinSalida a '0'
 			{	
 				digitalWrite(PinSalida, LOW);	
 			}
 			if (oMensaje.Mensaje == "Change")					//Si se recibe 'Change', cambia el estado de PinSalida 
 			{	
 				if ( digitalRead(PinSalida) == 1)
 				{
 					digitalWrite(PinSalida, LOW);
 				}else{
 					digitalWrite(PinSalida, HIGH);
 				}
 			}
 			if (oMensaje.Mensaje == "ChangeGet")					//Si se recibe 'ChangeGet', cambia el estado de PinSalida y devuelve el nuevo estado al remitente 
 			{	
 				if ( digitalRead(PinSalida) == 1)
 				{
 					digitalWrite(PinSalida, LOW);
 					cSalida = "Off";
 				}else{
 					digitalWrite(PinSalida, HIGH);
 					cSalida = "On";					
 				}
 				oMensaje.Mensaje = cSalida;					//Confeccionamos el mensaje a enviar hacia el servidor	
 				oMensaje.Destinatario = oMensaje.Remitente;
 				EnviaMensaje(oMensaje);						//Y lo enviamos
 			}			
 			if (oMensaje.Mensaje == "Get")						//Si se recibe 'Get', se devuelve el estado de PinSalida al remitente
 			{	
 				if ( digitalRead(PinSalida) == 1)
 				{
 					cSalida = "On";
 				}else{
 					cSalida = "Off";
 				}
 				oMensaje.Mensaje = cSalida;
 				oMensaje.Destinatario = oMensaje.Remitente;
 				EnviaMensaje(oMensaje);	
 			}		

 			-----------------------
 			Actualización WebSocket
 			-----------------------

			if ( cSalida != String(' ') )						//Si hay texto en cSalida
			{	
				EnviaMensajeWebSocket(cSalida);					//Actualizamos las páginas html conectadas
			}

	 		--------------------
 			Notificacion HomeKit
 			--------------------
	 		
			#ifdef HomeKit								//Si esta definido HomeKit, enviamos información al dispositivo espejo
				if ( cSalida != String(' ') && oMensaje.Remitente != ( cDispositivo + String("_") ) )
				{
					oMensaje.Destinatario = cDispositivo + String("_");
					oMensaje.Mensaje = cSalida;
					EnviaMensaje(oMensaje);
				}
			#endif		
			
			cSalida = String(' ');			
		}
		wdt_reset();			 			
 			
 	}	

 *\endcode
 *
 *
 
//-------------------------------------------------------
//Pagina2 Modulos Especificos
//-------------------------------------------------------
/**
 * \page Outlet 3.2.1.- Módulo Outlet
 *
 */









