/**
******************************************************
* @file PagHistoria.h
* @brief Un poco de historia
* @author FabLab Lleida
* @author Jose Martinez Molero
* @author Julian Salas Bartolome
* @version 1.0
* @date Abril 2018
*
*
*******************************************************/

/**
 *
 * \section Historia1 1.- Un poco de historia
 * 
 * En realidad este proyecto se inici� hace ya unos cuantos a�os, ten�amos la inquietud de telecontrolar distintos dispositivos del hogar con electr�nica dise�ada por nosotros para poderla personalizar a nuestras necesidades
 * y para que resultara lo mas econ�mica posible.
 *
 * En esa �poca, todav�a no se comercializaba el ESP8266, los primeros intentos los realizamos con el modulo RN-171, un buen dispositivo pero excesivamente caro para utilizarlo de forma masiva en un sistema.
 *
 * \image html Documentacion\Imagenes\RN-171.jpg
 *
 * El precio del RN-171 nos hizo buscar dispositivos que fueran mas econ�micos y, descubrimos y probamos con �xito el HLK-RM04 de Hi-Link: Este transceptor WIFI es relativamente peque�o  pero necesitaba un microcontrolador para poderlo gestionar.
 * Dise�amos una placa con el PIC 18F2550 y conseguimos unos resultados gratificantes aunque el dispositivo final era algo voluminoso por que, a la placa ( 50 x 50 mm ), se le debia a�adir una fuente de alimentaci�n.
 * 
 * Es un modulo con muchas posibilidades, ademas de WIFI gestiona tambien puertos Ethernet, incluye una web de configuracion sin embargo no conseguimos hacerlo lo suficientemente reducido para nuestras necesidades.
 *
 * \image html Documentacion\Imagenes\HLK-RM04.jpg
 *
 * Nos pusimos a buscar soluciones que nos permitieran dise�ar dispositivos de control dom�tico mas peque�os, nos encontramos entonces con el modulo Linkit 7681 de Meidatec Labs.
 * 
 * \image html Documentacion\Imagenes\Linkit7681.jpg
 *
 * Un m�dulo de reducidas dimensiones y lo mas importante es que dentro del mismo se incluye un microcontrolados con varios pines de  Entradas/Salidas. Parecia la soluci�n ideal
 *
 * Mediatek Labs ofrece para este dispositivo una placa de desarrollo y un SDK para su programaci�n. Los resultados fueron tambien satisfactorios.
 * Dedicamos muchas horas al estudio de este dispositivo, dise�amos una placa de aplicaci�n con la fuente de alimentaci�n integrada, el tama�o final era adecuado sin embargo, para nuestro gusto, la programaci�n era bastante farragosa
 * y no supimos hacerlo todo lo flexibe que deseabamos.
 *
 * Al mismo tiempo que dedicabamos esfuerzo al modulo Linkit 7681 circulaba ya de forma masiva el ESP8266. Al principio eramos reacios a utilizar ese m�dulo, era volver a invertir un monton de horas en otros dispositivo del que no terminabamos de ver todas sus virtudes.
 *
 * El primer contacto que tuvimos con el ESP8266 modelo EP-01 fue utiliz�ndolo con comandos AT, era un m�dulo peque�o pero volv�aamos a necesitar un micro por lo que se descart� esa posibilidad ya que tambien exist�a la posibilidad de acceder al microcontrolador interno
 * mediante lenguaje LUA. 
 *
 * \image html Documentacion\Imagenes\Esp8266.png
 *

 * LUA parec�a bastante asequible para utilizaciones b�sicas pero no dejaba de ser un nuevo lenguaje que necesitaba sus horas de aprendizaje. 
 *
 * A todo esto, personalmente, siempre habia esquivado el mundo Arduino, historicamente habia utilizao los Pics de Microchip, ten�a abundante documentaci�n, equipos de desarrollo y bastante conocimiento sobre ellos, los Pic cubr�an sobradamente mis necesidades
 * y no me apetec�a embarcarme en otras paltaformas. Las circunstancias y la casualidad hicieron que al final sucumbiera y me apuntara aun curso de Arduino que me permiti� conocer las posibilidades que ofrecia esta popular plataforma sin renunciar a mis queridos Pic.
 * Acabado el curso de Arduino.... se produjo el milagro, descubrimos que era posible utilizar los modulos ESP8266 como si fueran un Arduino. Durante un curso acad�mico hab�amos aprendido lo suficiente para empezar a defendernos con Arduino y ese conocimiento  
 * lo pod�amos aplicar al ESP8266 con poco esfuerzo. Se le adjudic� al ESP8266 la responsabiidad de ser el elemento b�sico de los dispositivos de nuestro sistema domotico.
 *
 * A todo esto, en las pruebas realizadas con los distintos modulos los conectabamos a la red wifi dom�stica y conectabamos con ellos a traves de su IP.
 *
 * Este modo de telecontrol estaba bien para un dispositivo pero no era adecuado para un sistema que se pretend�a telecontrolar incluso fuera del hogar. 
 *
 * Para poder telecontrolar un sistema con varios m�dulos se creo ServerPic. Los primeros m�dulos para los que se trabajo fueron los basados en HLK-RM04 y Pic, de ah� el  nombre. ServerPic premite la conexi�n entre distintos m�dulos
 * y la comunicaci�nn entre ellos sin tener la necesidad de conocer sus IP para poderlos comandar. ServerPic, al igual que los m�dulos Wifi, se conectan a la SSID de la red dom�stica, posteriormente, cada m�dulo Wifi se conecta a ServerPic
 * con un nombre individualizado, este nombre ser� el que se utililzar� cuando nos queramos dirigir a un m�dulo o cuando otro m�dulo requiera su intervenci�n.
 *  
 * Somos conscientes de que existen herramientas populares ( MQT por ejemplo ) que hacen algo similar a ServerPic pero se ha optado por esta opcion por que es propia, se puede personalizar, ampliar ssegun necesidades y es compatible con cualquier terminal que pueda abrir un socket.
 *
 * Realizado este paseo por la historia podemos continuar con el siguiente apartado \ref ServerPic1 o ir al \ref Indice  
 */
//-------------------------------------------------------
//Pagina1 Historia
//-------------------------------------------------------
/**
 * \page Historia 1.-Introducci�n
 *
 */